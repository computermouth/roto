#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

// Forward declarations
class QLabel;

class preferences_dialog: public QDialog
{
	Q_OBJECT

private slots:
	void set_canvas_width_slot();
	void set_canvas_height_slot();
	void set_off_canvas_width_slot();
	void set_off_canvas_height_slot();
public:
	void set_canvas_width(int);
	int get_canvas_width();

	void set_canvas_height(int);
	int get_canvas_height();

	void set_off_canvas_width(int);
	int get_off_canvas_width();

	void set_off_canvas_height(int);
	int get_off_canvas_height();

private:
	int m_canvas_width;
	int m_canvas_height;
	int m_off_canvas_width;
	int m_off_canvas_height;

	QLabel* m_canvas_width_label;
	QLabel* m_canvas_height_label;
	QLabel* m_off_canvas_width_label;
	QLabel* m_off_canvas_height_label;
public:
	preferences_dialog(QWidget *parent = 0);

	~preferences_dialog();

};

#endif
