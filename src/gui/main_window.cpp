#include "graphics_scene.h"
#include "list_widget.h"
#include "main_window.h"
#include "preferences_dialog.h"

#include <db/animation.h>
#include <db/frame.h>
#include <db/layer.h>
#include <db/manager.h>
#include <engine/conf_saver.h>
#include <engine/conf_restorer.h>
#include <engine/open_project.h>
#include <engine/saver.h>
#include <engine/player.h>

#include <QApplication>
#include <QFileDialog>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QSplitter>
#include <QTemporaryFile>
#include <QWidget>

#include <cassert>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>

bool main_window::
message_dialog(const std::string& t, const std::string& m)
{
	QMessageBox msgBox;
	msgBox.setWindowTitle(t.c_str());
	msgBox.setText(m.c_str());
	msgBox.setStandardButtons(QMessageBox::Yes);
	msgBox.addButton(QMessageBox::No);
	msgBox.setDefaultButton(QMessageBox::No);
	if(msgBox.exec() == QMessageBox::Yes){
		return true;
	}
	return false;
}

int main_window::
save_dialog_message(
	const std::string& w,
	const std::string& t,
	const std::string& m)
{
	QMessageBox msgBox;
	msgBox.setWindowTitle(w.c_str());
	msgBox.setText(t.c_str());
	msgBox.setInformativeText(m.c_str());
	msgBox.setStandardButtons(
			QMessageBox::Save |
			QMessageBox::Cancel |
			QMessageBox::Ignore);
	msgBox.setDefaultButton(QMessageBox::Save);
	return msgBox.exec();
}

void main_window::
refresh()
{
	m_animations_list->refresh();
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	roto::db::manager::animations* a = m->animations_get();
	roto::db::manager::animations::const_iterator b = a->begin();
	for (; b != a->end(); ++b) {
		m_frames_list->refresh(*b);
		const roto::db::animation::frames* f = (*b)->frames_get(); assert(0 != f);
		roto::db::animation::frames::const_iterator t = f->begin();
		for (; t != f->end(); ++t) {
			m_layers_list->refresh(*t);
		}
	}
}

void main_window::
connect_signals()
{
	connect(m_animations_list->get_list(), SIGNAL(itemSelectionChanged()),
		       	this, SLOT(on_animations_selection()));
	connect(m_frames_list->get_list(), SIGNAL(itemSelectionChanged()),
		       	this, SLOT(on_frames_selection()));
	connect(m_layers_list->get_list(), SIGNAL(itemSelectionChanged()),
		       	this, SLOT(on_layers_selection()));

	connect(m_draw_btn, SIGNAL(clicked()),
		       	this, SLOT(on_draw_selection()));
	connect(m_select_btn, SIGNAL(clicked()),
		       	this, SLOT(on_select_selection()));
	connect(m_remove_btn, SIGNAL(clicked()),
		       	this, SLOT(on_remove_selection()));

	connect(m_zoom_in, SIGNAL(clicked()),
			this, SLOT(on_zoom_in_selection()));
	connect(m_zoom_out, SIGNAL(clicked()),
			this, SLOT(on_zoom_out_selection()));
	connect(m_zoom_fit, SIGNAL(clicked()),
			this, SLOT(on_zoom_fit_selection()));


	connect(this, SIGNAL(select_frames_row(int)),
		       	this, SLOT(on_select_frames_row(int)));
	connect(this, SIGNAL(stop_player()),
		       	this, SLOT(on_play_stop()));
}

void main_window::
on_animations_selection()
{
	m_frames_list->clear();
	m_layers_list->clear();
	QGraphicsView* v = get_graphics_view(); assert(0 != v);
	v->setScene(NULL);
	QListWidgetItem* w = m_animations_list->get_list()->currentItem();
	if (0 != w) {
		QVariant v;
		v = w->data(Qt::UserRole);
		const roto::db::animation* f = v.value<const roto::db::animation*>();
		m_frames_list->refresh(f);
		if (f->frames_get()->size() > 0) {
			m_frames_list->get_list()->setCurrentRow(0);
		}
	}
}

void main_window::
on_frames_selection()
{
	m_layers_list->clear();
	QGraphicsView* v = get_graphics_view(); assert(0 != v);
	v->setScene(NULL);
	QListWidgetItem* w = m_frames_list->get_list()->currentItem();
	if (0 != w) {
		QVariant d;
		d = w->data(Qt::UserRole);
		assert(0 != d.value<const roto::db::frame*>());
		const roto::db::frame* f = d.value<const roto::db::frame*>();
		m_layers_list->refresh(f);
		roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
		main_window* w = m->get_main_window(); assert(0 != w);
		QGraphicsView* v = w->get_graphics_view(); assert(0 != v);
		graphics_scene* g = f->get_graphics_scene(); assert(0 != g);
		v->setScene((QGraphicsScene*)g);
		if (f->layers_get()->size() > 0) {
			m_layers_list->get_list()->setCurrentRow(0);
		}
	}
}

void main_window::
on_layers_selection()
{
	QListWidgetItem* w = m_layers_list->get_list()->currentItem();
	if (0 != w) {
		QVariant v;
		v = w->data(Qt::UserRole);
		assert(0 != v.value<const roto::db::layer*>());
		const roto::db::layer* l = v.value<const roto::db::layer*>();
		roto::db::frame* f = l->get_owner_frame(); assert(0 != f);
		f->set_active_layer(l);

		//roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
		//main_window* w = m->get_main_window(); assert(0 != w);
		graphics_scene* g = f->get_graphics_scene(); assert(0 != g);
		g->set_layer();
		if (l->is_image()) {
			m_draw_btn->setDisabled(true);
			m_remove_btn->setDisabled(true);
			on_select_selection();
		} else {
			m_draw_btn->setDisabled(false);
			m_remove_btn->setDisabled(false);
			m_select_btn->setDisabled(false);
		}
	}
}

void main_window::
on_draw_selection()
{
	if (m_select_btn->isChecked()) {
		m_select_btn->toggle();
	}
	if (m_remove_btn->isChecked()) {
		m_remove_btn->toggle();
	}
	if (! m_draw_btn->isChecked()) {
		m_draw_btn->toggle();
	}
	QGraphicsScene* t = m_view->scene();
	if (0 == t) {
		return;
	}
	graphics_scene* s = dynamic_cast<graphics_scene*>(t); assert(0 != s);
	s->set_layer();
	//s->draw_polygon();
}

void main_window::
on_select_selection()
{
	if (m_draw_btn->isChecked()) {
		m_draw_btn->toggle();
	}
	if (m_remove_btn->isChecked()) {
		m_remove_btn->toggle();
	}
	if (! m_select_btn->isChecked()) {
		m_select_btn->toggle();
	}
}

void main_window::
on_remove_selection()
{
	if (m_draw_btn->isChecked()) {
		m_draw_btn->toggle();
	}
	if (m_select_btn->isChecked()) {
		m_select_btn->toggle();
	}
	if (! m_remove_btn->isChecked()) {
		m_remove_btn->toggle();
	}
}

void main_window::
on_zoom_in_selection()
{
	qreal factor = 1.2;
	m_zoom_factor *= factor;
	m_view->scale(factor, factor);
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	m->set_zoom_factor(m_zoom_factor);
	QGraphicsScene* t = m_view->scene();
	if (0 == t) {
		return;
	}
	graphics_scene* s = dynamic_cast<graphics_scene*>(t); assert(0 != s);
	s->redraw_frame_polygons();
}

void main_window::
on_zoom_out_selection()
{
	qreal factor = 0.8;
	m_zoom_factor *= factor;
	m_view->scale(factor, factor);
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	m->set_zoom_factor(m_zoom_factor);
	QGraphicsScene* t = m_view->scene();
	if (0 == t) {
		return;
	}
	graphics_scene* s = dynamic_cast<graphics_scene*>(t); assert(0 != s);
	s->redraw_frame_polygons();
}

void main_window::
on_zoom_fit_selection()
{
	m_view->scale(1/m_zoom_factor, 1/m_zoom_factor);
	m_zoom_factor = 1.0;
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	m->set_zoom_factor(m_zoom_factor);
	QGraphicsScene* t = m_view->scene();
	if (0 == t) {
		return;
	}
	graphics_scene* s = dynamic_cast<graphics_scene*>(t); assert(0 != s);
	s->redraw_frame_polygons();
}

void main_window::
on_select_frames_row(int r)
{
	if (m_frames_list != 0 && m_frames_list->get_list()->count() > 0) {
		m_frames_list->get_list()->setCurrentRow(r);
	}
}

void main_window::
copy_animation()
{
	roto::db::animation* a = get_selected_animation();
	if (0 != a) {
		roto::db::animation* c = new roto::db::animation(*a);
		c->set_name(c->get_name() + "_copy");
		roto::db::manager::get()->add_animation(c);
		get_animations_list()->refresh();
		get_animations_list()->get_list()->setFocus();
		if (get_animations_list()->get_list()->count() > 0) {
			get_animations_list()->get_list()->setCurrentRow(get_animations_list()->get_list()->count() - 1);
		}
	}
}

void main_window::
copy_frame()
{
	roto::db::frame* a = get_selected_frame();
	if (0 != a) {
		roto::db::frame* c = new roto::db::frame(*a);
		c->set_name(c->get_name() + "_copy");
		get_selected_animation()->add_frame(c);
		get_frames_list()->refresh(get_selected_animation());
		get_frames_list()->get_list()->setFocus();
		if (get_frames_list()->get_list()->count() > 0) {
			get_frames_list()->get_list()->setCurrentRow(get_frames_list()->get_list()->count() - 1);
		}
	}
}

void main_window::
copy_layer()
{
	roto::db::layer* a = get_selected_layer();
	if (0 != a) {
		roto::db::layer* c = new roto::db::layer(*a);
		c->set_name(c->get_name() + "_copy");
		get_selected_frame()->add_layer(c);

		get_layers_list()->refresh(get_selected_frame());
		get_layers_list()->get_list()->setFocus();
		if (get_layers_list()->get_list()->count() > 0) {
			get_layers_list()->get_list()->setCurrentRow(get_layers_list()->get_list()->count() - 1);
		}
	}
}

void main_window::
batch_import()
{
	roto::db::animation* a = get_selected_animation();
	if (0 != a) {
		QStringList fl = QFileDialog::getOpenFileNames(this,
				tr("Select Image"),
				"./",
				tr("Image (*.jpg *.jpeg *.png)"));
		if (fl.isEmpty()) {
			return;
		}

		for (int i = 0; i < fl.count(); ++i) {
			std::string p = fl.at(i).toStdString();
			std::stringstream n;
			n << "bi_" << i;
			roto::db::frame* f = a->add_frame(n.str(), 2); assert(0 != f);
			roto::db::layer* l = f->add_layer(n.str()); assert(0 != l);
			l->set_image_path(p);
			l->create_image_item();
		}
		get_animations_list()->refresh();
		get_animations_list()->get_list()->setFocus();
		if (get_animations_list()->get_list()->count() > 0) {
			get_animations_list()->get_list()->setCurrentRow(get_animations_list()->get_list()->count() - 1);
		}
	}
}

void main_window::
preferences()
{
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	preferences_dialog* p = m->get_preferences_dialog();
	p->exec();
	conf_saver();
	restore_gui_properties();
}

void main_window::
on_play_stop()
{
	m_animations_list->setDisabled(false);
	m_frames_list->setDisabled(false);
	m_layers_list->setDisabled(false);
	if (0 != m_player) {
		if (m_player->isRunning()) {
			m_player->terminate();
			m_player->quit();
			m_player->wait();
		}
		delete m_player;
		m_player = 0;
	}
}

void main_window::
create_content()
{
	QWidget* widget = new QWidget;
	setCentralWidget(widget);
	QHBoxLayout* layout = new QHBoxLayout;
	layout->setMargin(5);

	m_splitter = new QSplitter();
	m_splitter->setChildrenCollapsible(false);
	layout->addWidget(m_splitter);
	m_animations_list = new list_widget(tr("Animations"), global::animation, this);
	m_splitter->addWidget(m_animations_list);
	m_frames_list = new list_widget(tr("Frames"), global::frame, this);
	m_splitter->addWidget(m_frames_list);
	m_layers_list = new list_widget(tr("Layers"), global::layer, this);
	m_splitter->addWidget(m_layers_list);

	m_view = new QGraphicsView();
	m_view->setStyleSheet("background: transparent");

        m_view->setBackgroundBrush(QBrush(Qt::white, Qt::SolidPattern));

	QSizePolicy policy = m_view->sizePolicy();
	policy.setHorizontalStretch(1);
	policy.setVerticalStretch(1);
	m_view->setSizePolicy(policy);

	QHBoxLayout* s = new QHBoxLayout();
	QVBoxLayout* b = new QVBoxLayout();

	m_draw_btn = new QPushButton();
	m_select_btn = new QPushButton();
	m_remove_btn = new QPushButton();

	m_zoom_in = new QPushButton();
	m_zoom_out = new QPushButton();
	m_zoom_fit = new QPushButton();

	m_draw_btn->setIcon(QIcon(":/icons/pen-2-32.png"));
	m_select_btn->setIcon(QIcon(":/icons/omnidirectional-drag-32.png"));
	m_remove_btn->setIcon(QIcon(":/icons/remove.png"));

	m_zoom_in->setIcon(QIcon(":/icons/zoom-in-2-32.png"));
	m_zoom_out->setIcon(QIcon(":/icons/zoom-out-2-32.png"));
	m_zoom_fit->setIcon(QIcon(":/icons/fit-to-width-32.png"));

	m_draw_btn->setCheckable(true);
	m_select_btn->setCheckable(true);
	m_remove_btn->setCheckable(true);

	m_draw_btn->toggle();

	b->addWidget(m_draw_btn);
	b->addWidget(m_select_btn);
	b->addWidget(m_remove_btn);

	b->addWidget(m_zoom_in);
	b->addWidget(m_zoom_out);
	b->addWidget(m_zoom_fit);

	b->addStretch(1);
	s->addWidget(m_view);
	s->addLayout(b);
	QWidget* w = new QWidget();
	w->setLayout(s);
	m_splitter->addWidget(w);

	widget->setLayout(layout);
	m_animations_list->refresh();
}

void main_window::
create_actions()
{
	m_open = new QAction(tr("&Open"), this);
	m_open->setShortcuts(QKeySequence::Open);
	m_open->setStatusTip(tr("Open project"));
	connect(m_open, SIGNAL(triggered()), this, SLOT(open()));

	m_save = new QAction(tr("&Save"), this);
	m_save->setShortcuts(QKeySequence::Save);
	m_save->setStatusTip(tr("Save the project"));
	connect(m_save, SIGNAL(triggered()), this, SLOT(save()));

	m_save_as = new QAction(tr("&Save As..."), this);
	m_save_as->setShortcuts(QKeySequence::SaveAs);
	m_save_as->setStatusTip(tr("Save as the project"));
	connect(m_save_as, SIGNAL(triggered()), this, SLOT(save_as()));

	m_quit = new QAction(tr("&Quit"), this);
	m_quit->setShortcuts(QKeySequence::Quit);
	m_quit->setStatusTip(tr("Quit application"));
	connect(m_quit, SIGNAL(triggered()), this, SLOT(exit()));

	m_play = new QAction(tr("&Play"), this);
	//m_play->setShortcuts(QKeySequence::Play);
	m_play->setStatusTip(tr("Play animation"));
	connect(m_play, SIGNAL(triggered()), this, SLOT(play()));

	m_play_twice_speed = new QAction(tr("Play &2x"), this);
	//m_play_twice_speed->setShortcuts(QKeySequence::Play);
	m_play_twice_speed->setStatusTip(tr("Play animation"));
	connect(m_play_twice_speed, SIGNAL(triggered()),
		       	this, SLOT(play_twice_speed()));

	m_play_half_speed = new QAction(tr("Play &0.5x"), this);
	//m_play_half_speed->setShortcuts(QKeySequence::Play);
	m_play_half_speed->setStatusTip(tr("Play animation"));
	connect(m_play_half_speed, SIGNAL(triggered()),
		       	this, SLOT(play_half_speed()));

	m_stop_play = new QAction(tr("Stop"), this);
	m_stop_play->setStatusTip(tr("Stop animation"));
	connect(m_stop_play, SIGNAL(triggered()), this, SLOT(on_play_stop()));

	m_copy_animation = new QAction(tr("Duplicate Animation"), this);
	m_copy_animation->setStatusTip(tr("Duplicate animation"));
	connect(m_copy_animation, SIGNAL(triggered()),
			this, SLOT(copy_animation()));

	m_copy_frame = new QAction(tr("Duplicate Frame"), this);
	m_copy_frame->setStatusTip(tr("Duplicate frame"));
	connect(m_copy_frame, SIGNAL(triggered()),
			this, SLOT(copy_frame()));

	m_copy_layer = new QAction(tr("Duplicate Layer"), this);
	m_copy_layer->setStatusTip(tr("Duplicate layer"));
	connect(m_copy_layer, SIGNAL(triggered()),
			this, SLOT(copy_layer()));

	m_batch_import = new QAction(tr("Batch Import ..."), this);
	m_batch_import->setStatusTip(tr("Batch Import ..."));
	connect(m_batch_import, SIGNAL(triggered()),
			this, SLOT(batch_import()));

	m_preferences = new QAction(tr("&Preferences"), this);
	m_preferences->setShortcuts(QKeySequence::Preferences);
	m_preferences->setStatusTip(tr("Roto Preferences"));
	connect(m_preferences, SIGNAL(triggered()), this, SLOT(preferences()));

	m_about = new QAction(tr("&About"), this);
	//m_about->setShortcuts(QKeySequence::About);
	m_about->setStatusTip(tr("About Roto"));
	connect(m_about, SIGNAL(triggered()), this, SLOT(exit()));
}

void main_window::
create_menus()
{
	m_file_menu = menuBar()->addMenu(tr("&File"));
	m_file_menu->addSeparator();
	m_file_menu->addAction(m_open);
	m_file_menu->addAction(m_save);
	m_file_menu->addAction(m_save_as);
	m_file_menu->addAction(m_quit);

	m_animation_menu = menuBar()->addMenu(tr("&Animation"));
	m_animation_menu->addSeparator();
	m_animation_menu->addAction(m_play);
	m_animation_menu->addAction(m_play_twice_speed);
	m_animation_menu->addAction(m_play_half_speed);
	m_animation_menu->addAction(m_stop_play);


	m_edit_menu = menuBar()->addMenu(tr("&Edit"));
	m_edit_menu->addAction(m_copy_animation);
	m_edit_menu->addAction(m_copy_frame);
	m_edit_menu->addAction(m_copy_layer);
	m_edit_menu->addSeparator();
	m_edit_menu->addAction(m_batch_import);
	m_edit_menu->addSeparator();
	m_edit_menu->addAction(m_preferences);

	m_help_menu = menuBar()->addMenu(tr("&Help"));
	m_help_menu->addSeparator();
	m_help_menu->addAction(m_about);
}

bool main_window::
open()
{
	bool b = is_db_changed();
	if (b) {
		int c = save_dialog_message("Unsaved Changes",
			"The project has been modified.",
			"Do you want to save your changes?");
		switch (c) {
			case QMessageBox::Save:
				save();
				break;
			case QMessageBox::Ignore:
				break;
			case QMessageBox::Cancel:
				return true;
			default:
				// should never be reached
				break;
		}
	}
	QString file_name = QFileDialog::getOpenFileName(this,
		       	tr("Open File"),
			"./",
			tr("Yaml (*.yaml *.yml)"));
	if ("" == file_name) {
		return true;
	}
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);

	while (0 != m_animations_list->get_list()->count()) {
		m_animations_list->remove_item(0);
	}

	m_file_name = file_name;
	roto::engine::open_project* i =
	       	new roto::engine::open_project(m_file_name.toStdString());
	i->run();
	delete i;

	get_animations_list()->refresh();
	get_animations_list()->get_list()->setFocus();
	if (get_animations_list()->get_list()->count() > 0) {
		get_animations_list()->get_list()->setCurrentRow(get_animations_list()->get_list()->count() - 1);
	}

	std::string t = "rm -fr " + m_tmp_file_name;
	std::system(t.c_str());
	return true;
}

void main_window::
save()
{
	if ("" == m_file_name) {
		m_file_name = QFileDialog::getSaveFileName(this,
			       	tr("Save File"),
				"untitled.yaml",
				tr("Yaml (*.yaml *.yml)"));
	}
	if ("" == m_file_name) {
		return;
	}
	roto::engine::saver* i =
	       	new roto::engine::saver(m_file_name.toStdString());
	i->run();
	delete i;
}

void main_window::
save_as()
{
	m_file_name = QFileDialog::getSaveFileName(this, tr("Save File"),
		"untitled.yaml",
		tr("Yaml (*.yaml *.yml)"));
	if ("" == m_file_name) {
		return;
	}
	roto::engine::saver* i =
	       	new roto::engine::saver(m_file_name.toStdString());
	i->run();
	delete i;
}

bool main_window::
is_db_changed()
{
	std::string t = "rm -fr " + m_tmp_file_name;
	std::system(t.c_str());

	roto::engine::saver* i = new roto::engine::saver(m_tmp_file_name);
	i->run();
	delete i;
	std::fstream f1, f2;
	char c1, c2;
	f1.open(m_file_name.toStdString(), std::ios::in);
	f2.open(m_tmp_file_name, std::ios::in);
	bool flag = true;
	while(1) {
		c1=f1.get();
		c2=f2.get();
		if (c1 != c2) {
			flag = false;
			break;
		}
		if ((c1==EOF) || (c2==EOF)) {
			break;
		}
	}
	f1.close();
	f2.close();
	return !flag;
}

void main_window::
conf_saver()
{
	roto::engine::conf_saver* s = new roto::engine::conf_saver();
	s->run();
	delete s;
}

bool main_window::
exit()
{
	conf_saver();
	bool b = is_db_changed();
	if (b) {
		bool c = message_dialog("Unsaved Changes",
			"You have some unsaved changes. Are you sure you want to quit?");
		if (!c) {
			return false;
		}
	}
	std::string t = "rm -fr " + m_tmp_file_name;
	std::system(t.c_str());
	qApp->quit();
	return true;
}

void main_window::
play()
{
	on_play_stop();
	assert(0 == m_player);
	m_animations_list->setDisabled(true);
	m_frames_list->setDisabled(true);
	m_layers_list->setDisabled(true);
	m_player = new roto::engine::player(1);
	m_player->start();
}

void main_window::
play_twice_speed()
{
	on_play_stop();
	assert(0 == m_player);
	m_animations_list->setDisabled(true);
	m_frames_list->setDisabled(true);
	m_layers_list->setDisabled(true);
	m_player = new roto::engine::player(2);
	m_player->start();
}

void main_window::
play_half_speed()
{
	on_play_stop();
	assert(0 == m_player);
	m_animations_list->setDisabled(true);
	m_frames_list->setDisabled(true);
	m_layers_list->setDisabled(true);
	m_player = new roto::engine::player(0.5);
	m_player->start();
}

QGraphicsView* main_window::
get_graphics_view() const
{
	assert(0 != m_view);
	return m_view;
}

roto::db::animation* main_window::
get_selected_animation() const
{
	QListWidget* list = m_animations_list->get_list();
	int c = list->currentRow();
	if (c >= 0) {
		QListWidgetItem* item = list->item(c);
		QVariant v;
		v = item->data(Qt::UserRole);
		const roto::db::animation* a = v.value<const roto::db::animation*>();
		return const_cast<roto::db::animation*>(a);
	}
	return 0;
}

roto::db::frame* main_window::
get_selected_frame() const
{
	QListWidget* list = m_frames_list->get_list();
	int c = list->currentRow();
	if (c >= 0) {
		QListWidgetItem* item = list->item(c);
		QVariant v;
		v = item->data(Qt::UserRole);
		const roto::db::frame* a = v.value<const roto::db::frame*>();
		return const_cast<roto::db::frame*>(a);
	}
	return 0;
}

roto::db::layer* main_window::
get_selected_layer() const
{
	QListWidget* list = m_layers_list->get_list();
	int c = list->currentRow();
	if (c >= 0) {
		QListWidgetItem* item = list->item(c);
		QVariant v;
		v = item->data(Qt::UserRole);
		const roto::db::layer* a = v.value<const roto::db::layer*>();
		return const_cast<roto::db::layer*>(a);
	}
	return 0;
}

void main_window::
closeEvent (QCloseEvent* e)
{
	if (exit()) {
		e->accept();
	} else {
		e->ignore();
	}
}

void main_window::
restore_gui_properties()
{
	roto::engine::conf_restorer* i = new roto::engine::conf_restorer();
	i->run();
	delete i;
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	preferences_dialog* p = m->get_preferences_dialog();
	m_view->setFixedSize(
		p->get_off_canvas_width() + 2, p->get_off_canvas_height() + 2);
	m_view->update();
}

main_window::
main_window(QWidget *parent)
	: QMainWindow(parent)
	, m_file_name("")
	, m_tmp_file_name("")
	, m_player(0)
	, m_zoom_factor(1)
{
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
    	m->set_main_window(this);
	preferences_dialog* p = new preferences_dialog();	
	m->set_preferences_dialog(p);
	qApp->setStyleSheet(" QGroupBox { border: 1px solid lightGray; border-radius: 3px; } QGroupBox::title { background-color: transparent; subcontrol-position: top center;margin:5 13px;}");
	setWindowTitle(tr("Vector Graphics Editor"));
	setMinimumSize(900, 400);

	create_actions();
	create_menus();
	create_content();
	connect_signals();

	char s[] = "/tmp/roto_XXXXXX";
	mkstemp(s);
	m_tmp_file_name = std::string(s);

	restore_gui_properties();
}

main_window::
~main_window()
{
	delete m_animations_list;
	delete m_frames_list;
	delete m_layers_list;
	delete m_splitter;
}

