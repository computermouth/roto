#include <QtWidgets>

#include "preferences_dialog.h"
#include <iostream>

void preferences_dialog::
set_canvas_width(int v)
{
	m_canvas_width = v;
	m_canvas_width_label->setText(tr("%1").arg(m_canvas_width));
}

int preferences_dialog::
get_canvas_width()
{
	return m_canvas_width;
}

void preferences_dialog::
set_canvas_height(int v)
{
	m_canvas_height = v;
	m_canvas_height_label->setText(tr("%1").arg(m_canvas_height));
}

int preferences_dialog::
get_canvas_height()
{
	return m_canvas_height;
}

void preferences_dialog::
set_off_canvas_width(int v)
{
	m_off_canvas_width = v;
	m_off_canvas_width_label->setText(tr("%1").arg(m_off_canvas_width));
}

int preferences_dialog::
get_off_canvas_width()
{
	return m_off_canvas_width;
}

void preferences_dialog::
set_off_canvas_height(int v)
{
	m_off_canvas_height = v;
	m_off_canvas_height_label->setText(tr("%1").arg(m_off_canvas_height));
}

int preferences_dialog::
get_off_canvas_height()
{
	return m_off_canvas_height;
}

void preferences_dialog::
set_canvas_width_slot()
{
	bool b;
	m_canvas_width = QInputDialog::getInt(this,
			tr("Canvas Width"), tr(""),
			m_canvas_width, 0, 10000, 1, &b);
	if (b) {
		m_canvas_width_label->setText(
				QString::number(m_canvas_width));
	}
}

void preferences_dialog::
set_canvas_height_slot()
{
	bool b;
	m_canvas_height = QInputDialog::getInt(this,
			tr("Canvas Height"), tr(""),
			m_canvas_height, 0, 10000, 1, &b);
	if (b) {
		m_canvas_height_label->setText(
				QString::number(m_canvas_height));
	}
}

void preferences_dialog::
set_off_canvas_width_slot()
{
	bool b;
	m_off_canvas_width = QInputDialog::getInt(this,
			tr("Off-Canvas Width"), tr(""),
			m_off_canvas_width, 0, 10000, 1, &b);
	if (b) {
		m_off_canvas_width_label->setText(
				QString::number(m_off_canvas_width));
	}
}

void preferences_dialog::
set_off_canvas_height_slot()
{
	bool b;
	m_off_canvas_height = QInputDialog::getInt(this,
			tr("Off-Canvas Height"), tr(""),
			m_off_canvas_height, 0, 10000, 1, &b);
	if (b) {
		m_off_canvas_height_label->setText(
				QString::number(m_off_canvas_height));
	}
}

preferences_dialog::
preferences_dialog(QWidget *parent)
	: QDialog(parent)
	, m_canvas_width(800)
	, m_canvas_height(600)
	, m_off_canvas_width(1000)
	, m_off_canvas_height(800)
{
	QVBoxLayout *verticalLayout;
	if (QGuiApplication::styleHints()->showIsFullScreen()
			|| QGuiApplication::styleHints()->showIsMaximized()) {
		QHBoxLayout *horizontalLayout = new QHBoxLayout(this);
		QGroupBox *groupBox = new QGroupBox(QGuiApplication::applicationDisplayName(), this);
		horizontalLayout->addWidget(groupBox);
		verticalLayout = new QVBoxLayout(groupBox);
	} else {
		verticalLayout = new QVBoxLayout(this);
	}

	int frameStyle = QFrame::Sunken | QFrame::Panel;

	m_canvas_width_label = new QLabel;
	m_canvas_width_label->setFrameStyle(frameStyle);
	QPushButton* cw = new QPushButton(tr("Canvas Width:"));

	m_canvas_height_label = new QLabel;
	m_canvas_height_label->setFrameStyle(frameStyle);
	QPushButton* ch = new QPushButton(tr("Canvas Height:"));

	m_off_canvas_width_label = new QLabel;
	m_off_canvas_width_label->setFrameStyle(frameStyle);
	QPushButton* ocw = new QPushButton(tr("Off-Canvas Width:"));

	m_off_canvas_height_label = new QLabel;
	m_off_canvas_height_label->setFrameStyle(frameStyle);
	QPushButton* och = new QPushButton(tr("Off-Canvas Height:"));

	connect(cw, &QAbstractButton::clicked, this,
			&preferences_dialog::set_canvas_width_slot);
	connect(ch, &QAbstractButton::clicked, this,
			&preferences_dialog::set_canvas_height_slot);

	connect(ocw, &QAbstractButton::clicked, this,
			&preferences_dialog::set_off_canvas_width_slot);
	connect(och, &QAbstractButton::clicked, this,
			&preferences_dialog::set_off_canvas_height_slot);

	QWidget *page = new QWidget;
	QGridLayout *layout = new QGridLayout(page);
	layout->setColumnStretch(1, 1);
	layout->setColumnMinimumWidth(1, 250);
	layout->addWidget(cw, 0, 0);
	layout->addWidget(m_canvas_width_label, 0, 1);
	layout->addWidget(ch, 1, 0);
	layout->addWidget(m_canvas_height_label, 1, 1);
	layout->addWidget(ocw, 2, 0);
	layout->addWidget(m_off_canvas_width_label, 2, 1);
	layout->addWidget(och, 3, 0);
	layout->addWidget(m_off_canvas_height_label, 3, 1);
	layout->addItem(new QSpacerItem(0, 0, QSizePolicy::Ignored, QSizePolicy::MinimumExpanding), 5, 0);
	verticalLayout->addWidget(page);

	m_canvas_width_label->setText(tr("%1").arg(m_canvas_width));
	m_canvas_height_label->setText(tr("%1").arg(m_canvas_height));

	m_off_canvas_width_label->setText(tr("%1").arg(m_off_canvas_width));
	m_off_canvas_height_label->setText(tr("%1").arg(m_off_canvas_height));

	setWindowTitle("Preferences");
}

preferences_dialog::
~preferences_dialog()
{
}

