#include "color_dialog.h"

QColor color_dialog::get_color(QColor c)
{
	QColor color = QColorDialog::getColor(Qt::red, this);
	if (color.isValid()) {
		return color;
	}
	return c;
}
