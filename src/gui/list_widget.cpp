
// headers from this project
#include "graphics_scene.h"
#include "list_widget.h"
#include "main_window.h"

// headers from other projects
#include <db/animation.h>
#include <db/frame.h>
#include <db/manager.h>

// headers from QT libraries
#include <QCheckBox>
#include <QFileDialog>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>

// headers from standard libraries
#include <cassert>
#include <sstream>


int list_widget::m_animation_counter = 0;
int list_widget::m_frame_counter = 0;
int list_widget::m_layer_counter = 0;

bool list_widget::
message_dialog(const std::string& t, const std::string& m, size_t s)
{
	if (0 == s) {
		return true;
	}
	QMessageBox msgBox;
	msgBox.setWindowModality(Qt::ApplicationModal);
	msgBox.setWindowTitle(t.c_str());
	msgBox.setText(m.c_str());
	msgBox.setStandardButtons(QMessageBox::Yes);
	msgBox.addButton(QMessageBox::No);
	msgBox.setDefaultButton(QMessageBox::No);
	if(msgBox.exec() == QMessageBox::Yes){
		return true;
	}
	return false;
}

QListWidget* list_widget::
get_list() const
{
	return m_list;
}

void list_widget::
clear()
{
	assert(0 != m_list);
	m_list->clear();
}

void list_widget::
refresh()
{
	m_list->clear();
	roto::db::manager* manager = roto::db::manager::get();
	const roto::db::manager::animations* animations =
						manager->animations_get();
	roto::db::manager::animations::const_iterator i = animations->begin();
	for (; i != animations->end(); ++i) { 
		assert(0 != *i);
		QListWidgetItem* item = new QListWidgetItem(m_list);
		QVariant v;
		v.setValue(*i);
		item->setData(Qt::UserRole, v);
		m_list->addItem(item);
		QWidget* frameItem = new QWidget();
		QHBoxLayout* frameLayout = new QHBoxLayout();
		QHBoxLayout* hbox = new QHBoxLayout();
		QVBoxLayout* vbox = new QVBoxLayout();
		list_text* first = new list_text(this, list_text::name, m_list->count() - 1);
		first->setText(QString((*i)->get_name().c_str()));
		QLabel* loop = new QLabel(tr("Play loop"));
		list_checkbox* checkbox = new list_checkbox(this, m_list->count() - 1);
		checkbox->setChecked((*i)->is_loop());
		list_button* remove = new list_button(this, list_button::remove, m_list->count() - 1);
		remove->setIcon(QIcon(":/icons/remove.png"));
		vbox->addWidget(first);
		hbox->addWidget(loop);
		hbox->addWidget(checkbox);
		vbox->addLayout(hbox);
		frameLayout->addLayout(vbox);
		QWidget* spacer = new QWidget();
		spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored);
		frameLayout->addWidget(spacer);
		frameLayout->addWidget(remove);
		frameItem->setLayout(frameLayout);
		item->setSizeHint(frameItem->minimumSizeHint());
		m_list->setItemWidget(item, frameItem);
	} 
}

void list_widget::refresh(const roto::db::animation* a)
{
	assert(0 != m_list);
	m_list->clear();
	assert(0 != a);
	const roto::db::animation::frames* frames = a->frames_get();
	roto::db::animation::frames::const_iterator i = frames->begin();
	for (; i != frames->end(); ++i) { 
		assert(0 != *i);
		QListWidgetItem* item = new QListWidgetItem(m_list);
		QVariant v;
		v.setValue(*i);
		item->setData(Qt::UserRole, v);
		m_list->addItem(item);
		QWidget* frameItem = new QWidget();
		QHBoxLayout* frameLayout = new QHBoxLayout();
		list_text* first = new list_text(this, list_text::name, m_list->count() - 1);
		first->setText(QString((*i)->get_name().c_str()));
		std::stringstream s;
		s <<(*i)->get_delay();
		list_text* second = new list_text(this, list_text::delay, m_list->count() - 1);
		second->setText(QString(s.str().c_str()));
		list_button* remove = new list_button(this, list_button::remove, m_list->count() - 1);
		remove->setIcon(QIcon(":/icons/remove.png"));
		frameLayout->addWidget(first);
		frameLayout->addWidget(second);
		frameLayout->addWidget(remove);
		frameItem->setLayout(frameLayout);
		item->setSizeHint(frameItem->minimumSizeHint());
		m_list->setItemWidget(item, frameItem);
	} 
}

void list_widget::refresh(const roto::db::frame* f)
{
	assert(0 != f);
	m_list->clear();
	const roto::db::frame::layers* layers = f->layers_get();
	roto::db::frame::layers::const_iterator i = layers->begin();
	for (; i != layers->end(); ++i) { 
		QListWidgetItem* item = new QListWidgetItem(m_list);
		QVariant d;
		d.setValue(*i);
		item->setData(Qt::UserRole, d);
		m_list->addItem(item);
		QWidget* frameItem = new QWidget();
		QHBoxLayout *frameLayout = new QHBoxLayout();

		QLabel* image = new QLabel();
		roto::db::layer* l = const_cast<roto::db::layer*>(*i);
		l->set_image(image);

		QVBoxLayout* v = new QVBoxLayout();
		QHBoxLayout* h = new QHBoxLayout();
		list_button* color = new list_button(this,
				 list_button::color, m_list->count() - 1);
		if (l->is_image()) {
			color->hide();
		}

		color->setStyleSheet("QPushButton { background-color: "
				 + l->get_color().name() + "; }");

		//QPushButton* b = new QPushButton();
		list_button* b = new list_button(this,
				 list_button::eject, m_list->count() - 1);
		b->setIcon(QIcon(":/icons/eject-32.png"));
		list_button* remove = new list_button(this,
				 list_button::remove, m_list->count() - 1);
		remove->setIcon(QIcon(":/icons/remove.png"));
		list_text* first = new list_text(this,
				list_text::name, m_list->count() - 1);
		first->setText((*i)->get_name().c_str());
		v->addWidget(first);
		h->addWidget(color);
		h->addWidget(b);
		h->addStretch(1);
		v->addLayout(h);
		frameLayout->addLayout(v);
		frameLayout->addWidget(image);
		frameLayout->addWidget(remove);
		frameItem->setLayout(frameLayout);
		item->setSizeHint(frameItem->minimumSizeHint());
		m_list->setItemWidget(item, frameItem);
	} 
	graphics_scene* g = f->get_graphics_scene(); assert(0 != g);
	g->redraw_frame_polygons();
}

void list_widget::open_image_dialog()
{
	std::stringstream s;
	switch(m_type) {
	case global::animation: {
		assert(false);
		break;
	} case global::frame: {
		assert(false);
		break;
	} case global::layer: {

		s << "Layer_" << m_layer_counter;
		roto::db::frame* f = m_parent->get_selected_frame();
		if (0 != f) {
			QString i = QFileDialog::getOpenFileName(this,
					tr("Select Image"),
					"./",
					tr("Image (*.jpg *.jpeg *.png)"));
			if ("" == i) {
				return;
			}

			roto::db::layer* l = new roto::db::layer(s.str());
			f->add_layer(l);
			l->set_owner_frame(f);
			m_layer_counter++;
			l->set_image_path(i.toStdString());
			l->create_image_item();
			refresh(f);
		}
		break;
	} default:
		assert("Code should not be reached");
	}
	m_list->setFocus();
	if (m_list->count() > 0) {
		m_list->setCurrentRow(m_list->count() - 1);
	}
}

void list_widget::open_new_dialog()
{
/*
	new_dialog* d = new new_dialog("New", m_type, this);
	roto::db::manager* m = roto::db::manager::get();
	if (d->exec() == QDialog::Accepted) {
		switch(m_type) {
		case global::animation:
			m->add_animation(new roto::db::animation(d->get_name().toStdString()));
			refresh();
			break;
		case global::frame:
			break;
		case global::layer:
			break;
		default:
			assert("Code should not be reached");
		}
	}
*/
	roto::db::manager* m = roto::db::manager::get();
	std::stringstream s;
	switch(m_type) {
	case global::animation: {
		s << "Animation_" << m_animation_counter;
		m->add_animation(s.str());
		m_animation_counter++;
		refresh();
		break;
	} case global::frame: {
		s << "Frame_" << m_frame_counter;
		roto::db::animation* a = m_parent->get_selected_animation();
		if (0 != a) {
			a->add_frame(s.str(), 2);
			m_frame_counter++;
			refresh(a);
		}
		break;
	} case global::layer: {
		s << "Layer_" << m_layer_counter;
		roto::db::frame* f = m_parent->get_selected_frame();
		if (0 != f) {
			roto::db::layer* l = new roto::db::layer(s.str());
			f->add_layer(l);
			l->set_owner_frame(f);
			m_layer_counter++;
			refresh(f);
		}
		break;
	} default:
		assert("Code should not be reached");
	}
	m_list->setFocus();
	if (m_list->count() > 0) {
		m_list->setCurrentRow(m_list->count() - 1);
	}
}

void list_widget::select_top()
{
	int cr = m_list->currentRow();
	if (cr > 0) {
		QListWidgetItem* item = m_list->item(cr);
		QVariant v;
		v = item->data(Qt::UserRole);
		std::stringstream s;
		switch(m_type) {
		case global::animation: {
			const roto::db::animation* a = v.value<const roto::db::animation*>();
			roto::db::manager* manager = roto::db::manager::get();
			roto::db::manager::animations* animations =
			manager->animations_get();
			roto::db::manager::animations::iterator i;
			i = std::find(animations->begin(), animations->end(),a);
			animations->erase(i);
			i = animations->begin();
			animations->insert(i, a);
			refresh();
			break;
		} case global::frame: {
			const roto::db::frame* f = v.value<const roto::db::frame*>();
			roto::db::animation* a = m_parent->get_selected_animation();
			roto::db::animation::frames* frames = a->frames_get();
			roto::db::animation::frames::iterator i;
			i = std::find(frames->begin(), frames->end(),f);
			frames->erase(i);
			i = frames->begin();
			frames->insert(i, f);
			refresh(a);
			break;
		} case global::layer: {
			const roto::db::layer* l = v.value<const roto::db::layer*>();
			roto::db::frame* a = m_parent->get_selected_frame();
			roto::db::frame::layers* layers = a->layers_get();
			roto::db::frame::layers::iterator i;
			i = std::find(layers->begin(), layers->end(),l);
			layers->erase(i);
			i = layers->begin();
			layers->insert(i, l);
			refresh(a);
			break;
		} default:
			assert("Code should not be reached");
		}
		m_list->setFocus();
	}
}

void list_widget::select_up()
{
	int cr = m_list->currentRow();
	if (cr > 0) {
		QListWidgetItem* item = m_list->item(cr);
		QVariant v;
		v = item->data(Qt::UserRole);
		std::stringstream s;
		switch(m_type) {
		case global::animation: {
			const roto::db::animation* a = v.value<const roto::db::animation*>();
			roto::db::manager* manager = roto::db::manager::get();
			roto::db::manager::animations* animations =
			manager->animations_get();
			roto::db::manager::animations::iterator i;
			i = std::find(animations->begin(), animations->end(),a);
			roto::db::manager::animations::iterator u = i - 1;
			animations->erase(i);
			animations->insert(u, a);
			refresh();
			break;
		} case global::frame: {
			const roto::db::frame* f = v.value<const roto::db::frame*>();
			roto::db::animation* a = m_parent->get_selected_animation();
			roto::db::animation::frames* frames = a->frames_get();
			roto::db::animation::frames::iterator i;
			i = std::find(frames->begin(), frames->end(),f);
			roto::db::animation::frames::iterator u = i - 1;
			frames->erase(i);
			frames->insert(u, f);
			refresh(a);
			break;
		} case global::layer: {
			const roto::db::layer* f = v.value<const roto::db::layer*>();
			roto::db::frame* a = m_parent->get_selected_frame();
			roto::db::frame::layers* layers = a->layers_get();
			roto::db::frame::layers::iterator i;
			i = std::find(layers->begin(), layers->end(),f);
			roto::db::frame::layers::iterator u = i - 1;
			layers->erase(i);
			layers->insert(u, f);
			refresh(a);
			break;
		} default:
			assert("Code should not be reached");
		}
		m_list->setFocus();
		m_list->setCurrentRow(cr - 1);
	}
}

void list_widget::select_down()
{
	int cr = m_list->currentRow();
	if (cr >= 0 && cr < m_list->count() - 1) {
		QListWidgetItem* item = m_list->item(cr);
		QVariant v;
		v = item->data(Qt::UserRole);
		std::stringstream s;
		switch(m_type) {
		case global::animation: {
			const roto::db::animation* a = v.value<const roto::db::animation*>();
			roto::db::manager* manager = roto::db::manager::get();
			roto::db::manager::animations* animations =
			manager->animations_get();
			roto::db::manager::animations::iterator i;
			i = std::find(animations->begin(), animations->end(),a);
			roto::db::manager::animations::iterator u = i + 1;
			animations->erase(i);
			animations->insert(u, a);
			refresh();
			break;
		} case global::frame: {
			const roto::db::frame* f = v.value<const roto::db::frame*>();
			roto::db::animation* a = m_parent->get_selected_animation();
			roto::db::animation::frames* frames = a->frames_get();
			roto::db::animation::frames::iterator i;
			i = std::find(frames->begin(), frames->end(),f);
			roto::db::animation::frames::iterator u = i + 1;
			frames->erase(i);
			frames->insert(u, f);
			refresh(a);
			break;
		} case global::layer: {
			const roto::db::layer* l = v.value<const roto::db::layer*>();
			roto::db::frame* a = m_parent->get_selected_frame();
			roto::db::frame::layers* layers = a->layers_get();
			roto::db::frame::layers::iterator i;
			i = std::find(layers->begin(), layers->end(),l);
			roto::db::frame::layers::iterator u = i + 1;
			layers->erase(i);
			layers->insert(u, l);
			refresh(a);
			break;
		} default:
			assert("Code should not be reached");
		}
		m_list->setFocus();
		m_list->setCurrentRow(cr + 1);
	}
}

void list_widget::select_bottom()
{
	int cr = m_list->currentRow();
	if (cr >= 0) {
		QListWidgetItem* item = m_list->item(cr);
		QVariant v;
		v = item->data(Qt::UserRole);
		std::stringstream s;
		switch(m_type) {
		case global::animation: {
			const roto::db::animation* a = v.value<const roto::db::animation*>();
			roto::db::manager* manager = roto::db::manager::get();
			roto::db::manager::animations* animations =
			manager->animations_get();
			roto::db::manager::animations::iterator i;
			i = std::find(animations->begin(), animations->end(),a);
			animations->erase(i);
			i = animations->end();
			animations->insert(i, a);
			refresh();
			break;
		} case global::frame: {
			const roto::db::frame* f = v.value<const roto::db::frame*>();
			roto::db::animation* a = m_parent->get_selected_animation();
			roto::db::animation::frames* frames = a->frames_get();
			roto::db::animation::frames::iterator i;
			i = std::find(frames->begin(), frames->end(),f);
			frames->erase(i);
			i = frames->end();
			frames->insert(i, f);
			refresh(a);
			break;
		} case global::layer: {
			const roto::db::layer* l = v.value<const roto::db::layer*>();
			roto::db::frame* a = m_parent->get_selected_frame();
			roto::db::frame::layers* layers = a->layers_get();
			roto::db::frame::layers::iterator i;
			i = std::find(layers->begin(), layers->end(),l);
			layers->erase(i);
			i = layers->end();
			layers->insert(i, l);
			refresh(a);
			break;
		} default:
			assert("Code should not be reached");
		}
		m_list->setFocus();
		m_list->setCurrentRow(m_list->count() - 1);
	}
}

void list_widget::on_item_selection()
{
	assert(0 != m_index_label);
	assert(0 != m_arrays_length_label);
	std::stringstream s;
	switch(m_type) {
	case global::animation: {
		s << m_animation_counter;
		break;
	} case global::frame: {
		s << m_frame_counter;
		break;
	} case global::layer: {
		s << m_layer_counter;
		break;
	} default:
		assert("Code should not be reached");
	}
	m_arrays_length_label->setText(s.str().c_str());
	s.str("");
	s << (m_list->currentRow() + 1);
	m_index_label->setText(s.str().c_str());
}

void list_widget::create_content()
{
	QPushButton* button1 = new QPushButton();
	button1->setIcon(QIcon(":/icons/plus-32.png"));
	connect(button1, SIGNAL(clicked()), this, SLOT(open_new_dialog()));
	QPushButton* button2 = 0;
	if (global::layer == m_type) {
		button2 = new QPushButton();
		button2->setIcon(QIcon(":/icons/picture-3-32.png"));
		connect(button2, SIGNAL(clicked()),
			       	this, SLOT(open_image_dialog()));
	}
	QPushButton* button3 = new QPushButton();
	button3->setIcon(QIcon(":/icons/arrow-up-line-32.png"));
	connect(button3, SIGNAL(clicked()), this, SLOT(select_top()));
	QPushButton* button4 = new QPushButton();
	button4->setIcon(QIcon(":/icons/arrow-up-32.png"));
	connect(button4, SIGNAL(clicked()), this, SLOT(select_up()));
	QPushButton* button5 = new QPushButton();
	button5->setIcon(QIcon(":/icons/arrow-down-32.png"));
	connect(button5, SIGNAL(clicked()), this, SLOT(select_down()));
	QPushButton* button6 = new QPushButton();
	button6->setIcon(QIcon(":/icons/arrow-down-line-32.png"));
	connect(button6, SIGNAL(clicked()), this, SLOT(select_bottom()));
	m_index_label = new QLabel("0");
	QWidget* spacer = new QWidget();
	spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored);
	m_arrays_length_label = new QLabel("0");
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	setStyleSheet( "QListWidget::item { border: 1px solid lightGray; border-radius: 3px; }" );
	QVBoxLayout *vbox = new QVBoxLayout;
	QHBoxLayout *hbox = new QHBoxLayout;
	QHBoxLayout *hbox2 = new QHBoxLayout;
	vbox->addWidget(spacer);
	vbox->addWidget(spacer);
	vbox->addWidget(spacer);
	hbox->addWidget(button1);
	if (global::layer == m_type) {
		hbox->addWidget(button2);
	}
	hbox->addWidget(button3);
	hbox->addWidget(button4);
	hbox->addWidget(button5);
	hbox->addWidget(button6);
	hbox2->addWidget(m_index_label);
	hbox2->addWidget(spacer);
	hbox2->addWidget(m_arrays_length_label);
	hbox->addStretch(1);
	vbox->addLayout(hbox);
	vbox->addWidget(m_list);
	vbox->addLayout(hbox2);
	setLayout(vbox);
}

void list_widget::remove_item(int cr)
{
	if (cr >= 0) {
		QListWidgetItem* item = m_list->item(cr);
		QVariant v;
		v = item->data(Qt::UserRole);
		std::stringstream s;
		switch(m_type) {
		case global::animation: {
			const roto::db::animation* a =
				v.value<const roto::db::animation*>();
			size_t t = a->frames_get()->size();
			s << "Are you sure you want to delete \"" << a->get_name()
				<< "\" animation, it has " << t << " Frames.";
			bool c = message_dialog("Delete Animation?", s.str(), t);
			if (! c) {
				return;
			}
			m_list->takeItem(cr);
			roto::db::manager* manager = roto::db::manager::get();
			manager->remove_animation(a->get_name());
			--m_animation_counter;
			refresh();
			break;
		} case global::frame: {
			const roto::db::frame* f =
				v.value<const roto::db::frame*>();
			size_t t = f->layers_get()->size();
			s << "Are you sure you want to delete \"" << f->get_name()
				<< "\" frame, it has " << t << " Layers.";
			bool c = message_dialog("Delete Frame?", s.str(), t);
			if (! c) {
				return;
			}
			m_list->takeItem(cr);
			roto::db::animation* a =
				m_parent->get_selected_animation();
			m_list->clear();
			a->remove_frame(f->get_name());
			--m_frame_counter;
			refresh(a);
			break;
		} case global::layer: {
			const roto::db::layer* l =
				v.value<const roto::db::layer*>();
			m_list->takeItem(cr);
			roto::db::frame* f = m_parent->get_selected_frame();
			f->remove_layer(l->get_name());
			--m_layer_counter;
			refresh(f);
			break;
		} default:
			assert("Code should not be reached");
		}
		m_list->setFocus();
		if (m_list->count() > 0) {
			m_list->setCurrentRow(m_list->count() - 1);
		}
	}
}

list_widget::
list_widget(const QString& n, const global::type& t, main_window* w)
	: QGroupBox(n)
	, m_type(t)
	, m_parent(w)
{
	assert(global::max_type > m_type);
	m_list = new QListWidget();
	m_list->setSelectionMode(QAbstractItemView::SingleSelection);
	m_list->setStyleSheet("QListWidget::item::selected { background-color:#d5d5ff;}");
	create_content();
	connect(m_list, SIGNAL(itemSelectionChanged()),
		       	this, SLOT(on_item_selection()));
}

list_widget::
~list_widget()
{
}

