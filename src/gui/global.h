#ifndef ROTO_GUI_GLOBAL_H
#define ROTO_GUI_GLOBAL_H

namespace global {
enum type {
	animation=0,
	frame,
	layer,
	max_type };
}

#endif // ROTO_GUI_GLOBAL_H
