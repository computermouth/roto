#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <db/animation.h>
#include <db/frame.h>
#include <db/layer.h>
#include <engine/player.h>

#include <QGraphicsView>
#include <QSplitter>
#include <QGroupBox>
#include <QListWidget>
#include <QMainWindow>
#include <QPushButton>

class list_widget;
class graphics_scene;

class main_window : public QMainWindow
{
Q_OBJECT

private:
	// Menu item(s)
	QMenu* m_file_menu;
	QMenu* m_animation_menu;
	QMenu* m_edit_menu;
	QMenu* m_help_menu;

	// File menu item(s)
	QAction* m_open;
	QAction* m_save;
	QAction* m_save_as;
	QAction* m_quit;

	// Animation menu item(s)
	QAction* m_play;
	QAction* m_play_twice_speed;
	QAction* m_play_half_speed;
	QAction* m_stop_play;

	// Edit menu item(s)
	QAction* m_copy_animation;
	QAction* m_copy_frame;
	QAction* m_copy_layer;
	QAction* m_batch_import;
	QAction* m_preferences;

	// Help menu item(s)
	QAction* m_about;

	list_widget* m_animations_list;
	list_widget* m_frames_list;
	list_widget* m_layers_list;

	QGraphicsView* m_view;

	QSplitter* m_splitter;

public:
	void refresh();
public:
	QPushButton* m_draw_btn;
	QPushButton* m_select_btn;
	QPushButton* m_remove_btn;

	QPushButton* m_zoom_in;
	QPushButton* m_zoom_out;
	QPushButton* m_zoom_fit;
public:
	roto::db::animation* get_selected_animation() const;
	roto::db::frame* get_selected_frame() const;
	roto::db::layer* get_selected_layer() const;
	list_widget* get_animations_list() const {return m_animations_list;}
	list_widget* get_frames_list() const {return m_frames_list;}
	list_widget* get_layers_list() const {return m_layers_list;}
	QGraphicsView* get_graphics_view() const;
	QSplitter* get_splitter() {return m_splitter;}

private:
	void create_menus();
	void create_actions();
	void create_content();
	void connect_signals();
	bool is_db_changed();
	bool message_dialog(const std::string&, const std::string&);
	int save_dialog_message(const std::string& w,
			const std::string& t,
			const std::string& m);
	void closeEvent(QCloseEvent *);

signals:
	void select_frames_row(int);
	void stop_player();

private slots:
	bool open();
	void save();
	void save_as();
	bool exit();

	void play();
	void play_twice_speed();
	void play_half_speed();

	void on_animations_selection();
	void on_frames_selection();
	void on_layers_selection();
	void on_draw_selection();
	void on_select_selection();
	void on_remove_selection();
	void on_select_frames_row(int);
	void on_play_stop();

	void on_zoom_in_selection();
	void on_zoom_out_selection();
	void on_zoom_fit_selection();

	void copy_animation();
	void copy_frame();
	void copy_layer();
	void batch_import();
	void preferences();

	void restore_gui_properties();
	void conf_saver();

private:
	QString m_file_name;
	std::string m_tmp_file_name;
	roto::engine::player* m_player;
	qreal m_zoom_factor;

public:
	explicit main_window(QWidget *parent = 0);
	~main_window();
};

#endif // MAIN_WINDOW_H
