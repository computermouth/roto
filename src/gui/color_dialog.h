#ifndef ROTO_GUI_COLOR_DIALOG_H
#define ROTO_GUI_COLOR_DIALOG_H

#include <QColorDialog>
 
class color_dialog: public QWidget
{
public:
	QColor get_color(QColor);

};

#endif // ROTO_GUI_COLOR_DIALOG_H
