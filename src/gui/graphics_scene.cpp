
// headers from this project
#include "main_window.h"
#include "graphics_scene.h"
#include "list_widget.h"
#include "preferences_dialog.h"

// headers from other projects
#include "db/manager.h"
#include "db/point.h"
#include "db/layer.h"

// headers from QT libraries
#include <QDebug>
#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>
#include <QGraphicsSceneMouseEvent>

// headers from standard libraries
#include <cassert>
#include <iostream>

void graphics_scene::
draw_rectangle()
{
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	preferences_dialog* p = m->get_preferences_dialog();

	int xMax = p->get_canvas_width();
	int yMax = p->get_canvas_height();
	QPen* dashedLine = new QPen(QBrush(Qt::blue), 1, Qt::DashLine);
	QGraphicsLineItem* x0 = addLine(0, 0, 0, yMax, *dashedLine);
	QGraphicsLineItem* y0 = addLine(0, 0, xMax, 0, *dashedLine);
	QGraphicsLineItem* x1 = addLine(0, yMax, xMax, yMax, *dashedLine);
	QGraphicsLineItem* y1 = addLine(xMax, 0, xMax, yMax, *dashedLine);
	x0->setVisible(true);
	y0->setVisible(true);
	x1->setVisible(true);
	y1->setVisible(true);
}

void graphics_scene::
redraw_frame_polygons()
{
	typedef roto::db::frame::layers L;
	L* s = m_frame->layers_get(); assert(0 != s);
	L::iterator i = s->begin();
	for (; i != s->end(); ++i) {
		m_layer = const_cast<roto::db::layer*>(*i);
		add_new_points_of_polygon();
	}
	i = s->begin();
	for (; i != s->end(); ++i) {
		m_layer = const_cast<roto::db::layer*>(*i);
		m_layer->clear();
	}
	clear();
	i = s->begin();
	for (; i != s->end(); ++i) {
		m_layer = const_cast<roto::db::layer*>(*i);
		if (m_layer->is_image()) {
			m_layer->create_image_item();
		} else {
			remove_old_points_of_polygon();
		}
		if (m_layer->m_polygon_closed) {
			draw_polygon();
		} else {
			draw_path();
		}
	}
	draw_rectangle();
}

void graphics_scene::
add_new_points_of_polygon()
{
	typedef roto::db::layer::points P;
	P* p = m_layer->points_get();
	size_t s = p->size();
	QPolygonF* l = m_layer->get_polygon(); assert(0 != l);
	l->erase(l->begin(), l->end());

	s = p->size();
	for (size_t j = 0; j < s; ++j) {
		roto::db::point* t = p->at(j);
		QPointF o = t->get_point_center();
		roto::db::point* i = new roto::db::point(o);
		m_layer->points_add(i);
		if (m_layer->is_hided()) {
			i->hide();
		}
	}
	if (! m_layer->is_active()) {
		m_layer->set_inactive();
	}
}

void graphics_scene::
remove_old_points_of_polygon()
{
	typedef roto::db::layer::points P;
	P* p = m_layer->points_get();
	size_t s = p->size()/2;
	for (size_t j = 0; j < s; ++j) {
		P::const_iterator i = p->begin();
		m_layer->points_remove(*i);
	}
	for (size_t j = 0; j < s; ++j) {
		roto::db::point* t = p->at(j);
		addItem(t);
	}
}

void graphics_scene::
draw_polygon()
{
	if (0 == m_layer ) {
		return;
	}
	if (m_layer->is_image()) {
		QGraphicsPixmapItem* p = m_layer->get_image_item();
		p->setZValue(-1);
		addItem(p);
		return;
	}
	typedef roto::db::layer::points P;
	const P* p = m_layer->points_get();
	QPolygonF* l = m_layer->get_polygon(); assert(0 != l);
	if ((unsigned int)l->size() == p->size()) {
		return;
	}
	l->erase(l->begin(), l->end());
	P::const_iterator i = p->begin();
	for (int j = 0; i != p->end(); ++i, ++j) {
		if (m_layer == m_frame->get_active_layer() && m_selected_point_index == j) {
			(*i)->set_color("red");
		}
		*l << (*i)->get_point_center();
	}
	QBrush brush;
	brush.setColor(m_layer->get_color());
	brush.setStyle(Qt::SolidPattern);

	QPen pen(Qt::green);
	if (m_polygon_is_selected) {
		pen.setColor(Qt::yellow);
	}

	QGraphicsPolygonItem* t = addPolygon(*l, pen, brush);
	m_layer->set_polygon_item(t);
	t->setZValue(-1);
	if (m_layer->is_hided()) {
		t->hide();
	}

	QGraphicsScene scene;
	scene.addPolygon(*l, pen, brush);
	QImage image(QImage(scene.sceneRect().size().toSize(),
		QImage::Format_ARGB32)); 
	image.fill(Qt::transparent);                                            
	QPainter painter(&image);
	scene.render(&painter);
	QPixmap pm;
	pm.convertFromImage(image);
	QPixmap spm = pm.scaled(40, 40, Qt::KeepAspectRatio);
	m_layer->get_image()->setPixmap(spm);
}

void graphics_scene::
draw_path()
{
	if (0 == m_layer ) {
		return;
	}
	if (m_layer->is_image()) {
		QGraphicsPixmapItem* p = m_layer->get_image_item();
		p->setZValue(-1);
		addItem(p);
		return;
	}
	typedef roto::db::layer::points P;
	const P* p = m_layer->points_get();

	if (0 == p->size() || 1 == p->size()) {
		return;
	}
	P::const_iterator i = p->begin();
	QPainterPath pp((*i)->get_point_center());
	for (int j = 0; i != p->end(); ++i, ++j) {
		if (m_layer == m_frame->get_active_layer() && m_selected_point_index == j) {
			(*i)->set_color("red");
		}
		pp.lineTo((*i)->get_point_center());
	}
	QPen pen(Qt::green);
	roto::db::manager* m = roto::db::manager::get();
	double f = m->get_zoom_factor();
	pen.setWidthF(1/f);

	QGraphicsPathItem* l = addPath(pp, pen);
	m_layer->set_line_item(l);

	pen.setColor(Qt::red);
	QGraphicsScene scene;
	scene.addPath(pp, pen);
	QImage image(QImage(scene.sceneRect().size().toSize(),
		QImage::Format_ARGB32)); 
	image.fill(Qt::transparent);                                            
	QPainter painter(&image);
	scene.render(&painter);
	QPixmap pm;
	pm.convertFromImage(image);
	QPixmap spm = pm.scaled(40, 40, Qt::KeepAspectRatio);
	m_layer->get_image()->setPixmap(spm);
}

void graphics_scene::
draw_line(QGraphicsSceneMouseEvent* e)
{
	QPointF p = m_layer->points_get()->at(m_layer->points_get()->size()
			 - 1)->get_point_center();
	if(!itemToDraw){
	    itemToDraw = new QGraphicsLineItem;
	    this->addItem(itemToDraw);
	    itemToDraw->setPen(QPen(Qt::green, 1, Qt::SolidLine));
	    itemToDraw->setPos(p);
	}
	itemToDraw->setLine(0,0, e->scenePos().x() - p.x(),
			    e->scenePos().y() - p.y());
}



void graphics_scene::
mousePressEvent(QGraphicsSceneMouseEvent* e)
{

	m_mouse_is_released = false;
	set_layer();
	if (m_layer == 0) {
		return;
	}
	QGraphicsScene::mousePressEvent(e);
	m_mouse_pos = e->scenePos();
	roto::db::manager* manager = roto::db::manager::get();
	main_window* w = manager->get_main_window(); assert(0 != w);
	if (!w->m_select_btn->isChecked() && !m_layer->points_get()->empty()
			 && !w->m_remove_btn->isChecked()) {
		//draw_line(e);
		//redraw_frame_polygons();
	}
	if (w->m_select_btn->isChecked()) {
		typedef roto::db::layer::points P;
		P* v = m_layer->points_get();
		for (unsigned int i = 0; i < v->size(); ++i) {
			QGraphicsItem* j = v->at(i);
			roto::db::point* item = dynamic_cast<
				roto::db::point*>(j);
			if (0 != item && j->isUnderMouse() && m_layer == item->get_owner_layer()) {
				m_selected_point_index = i;
				m_polygon_is_selected = false;
				m_image_is_selected = false;
				return;
			}
		}
		QList<QGraphicsItem *> vs = items();
		for (int i = vs.size() - 1; i >= 0; --i) {
			QGraphicsItem* j = vs.at(i);
			QGraphicsPolygonItem* polygon =
				 dynamic_cast<QGraphicsPolygonItem*>(j);
			if (j->isUnderMouse() && 0 != polygon && polygon == m_layer->get_polygon_item()) {
				m_polygon_is_selected = true;
				m_selected_point_index = -1;
				m_image_is_selected = false;
				return;
			}
		}
		if (m_layer->is_image()) {
			QList<QGraphicsItem *> v = items();
			for (int i = 0; i < v.size(); ++i) {
				QGraphicsItem* j = v.at(i);
				QGraphicsPixmapItem* g =
					dynamic_cast<QGraphicsPixmapItem*>(j);
				if (0 != g && j->isUnderMouse() && m_layer->get_image_item() == g) {
					assert(m_layer->is_image());
					m_selected_point_index = -1;
					m_polygon_is_selected = false;
					m_image_is_selected = true;
					return;
				}
			}
		}
	}
	m_selected_point_index = -1;
	m_polygon_is_selected = false;
	m_image_is_selected = false;
}

void graphics_scene::
mouseMoveEvent(QGraphicsSceneMouseEvent* e)
{
	if (m_mouse_is_released) {
		return;
	}
	set_layer();
	if (0 == m_layer) {
		return;
	}
	roto::db::manager* manager = roto::db::manager::get();
	main_window* w = manager->get_main_window(); assert(0 != w);
	if (w->m_select_btn->isChecked()) {
		if (-1 != m_selected_point_index) {// && 0 != m_layer->get_polygon_item()) {
			QGraphicsScene::mouseMoveEvent(e);
			//QPolygonF* p = m_layer->get_polygon();
			//QPointF& k = const_cast<QPointF&>(p->at(m_selected_point_index));
			typedef roto::db::layer::points P;
			P* v = m_layer->points_get();
			QPointF k = v->at(m_selected_point_index)->get_point_center();
			QPointF f(k.x() + (e->scenePos().x() - m_mouse_pos.x()), k.y() + (e->scenePos().y() - m_mouse_pos.y()));
			v->at(m_selected_point_index)->set_point_center(f);
			//v->at(m_selected_point_index)->hide();
			//k.setX(e->scenePos().x());
			//k.setY(e->scenePos().y());
			m_mouse_pos = e->scenePos();
			redraw_frame_polygons();
			//update();
		}
		if (-1 == m_selected_point_index && 0 != m_layer->get_image_item() && m_image_is_selected) {
			QGraphicsScene::mouseMoveEvent(e);
			QPointF p = e->scenePos();
			QPointF k = m_layer->get_image_center();
			QPointF f(k.x() + (p.x() - m_mouse_pos.x()),
				  k.y() + (p.y() - m_mouse_pos.y()));
			m_layer->set_image_center(f);
			redraw_frame_polygons();
			m_mouse_pos = e->scenePos();
			return;
		}

		if (m_polygon_is_selected) {
			QGraphicsScene::mouseMoveEvent(e);
			QPolygonF* p = m_layer->get_polygon();
			for (int i = 0; i < p->size(); ++i) {
				QPointF& k = const_cast<QPointF&>(p->at(i));

				k.setX(k.x() + (e->scenePos().x() - m_mouse_pos.x()));
				k.setY(k.y() + (e->scenePos().y() - m_mouse_pos.y()));
			}
			typedef roto::db::layer::points P;
			P* v = m_layer->points_get();
			for (unsigned int i = 0; i < v->size(); ++i) {
				QPointF k = v->at(i)->get_point_center();
				QPointF f(k.x() + (e->scenePos().x() - m_mouse_pos.x()), k.y() + (e->scenePos().y() - m_mouse_pos.y()));
				v->at(i)->set_point_center(f);
				v->at(i)->hide();
			}
			m_mouse_pos = e->scenePos();
			update();
		}
	} else if (! m_layer->points_get()->empty()
			 && !w->m_remove_btn->isChecked()) {
		//draw_line(e);
		redraw_frame_polygons();
	}
}

void graphics_scene::
mouseReleaseEvent(QGraphicsSceneMouseEvent* e)
{
	itemToDraw = 0;
	m_mouse_pos = QPointF(-1,-1);
	m_mouse_is_released = true;
	set_layer();
	if (0 == m_layer) {
		return;
	}
	if (m_layer->is_hided()) {
		return;
	}
	roto::db::manager* manager = roto::db::manager::get();
	main_window* w = manager->get_main_window(); assert(0 != w);
	if (w->m_select_btn->isChecked()) {
		QPointF p = e->scenePos();
		QList<QGraphicsItem *> v = items();
		for (int i = 0; i < v.size(); ++i) {
			QGraphicsItem* j = v.at(i);
			if (j->isUnderMouse()) {
				roto::db::point* item = dynamic_cast<roto::db::point*>(j);
				if (0 != item && m_layer == item->get_owner_layer()) {
					QGraphicsScene::mouseReleaseEvent(e);
					item->set_point_center(p);
					m_polygon_is_selected = false;
					m_image_is_selected = false;
					redraw_frame_polygons();
					return;
				}
				QGraphicsPolygonItem* polygon =
					 dynamic_cast<QGraphicsPolygonItem*>(j);
				if (0 != polygon) {
					QGraphicsScene::mouseReleaseEvent(e);
					m_selected_point_index = -1;
					m_image_is_selected = false;
					typedef roto::db::layer::points P;
					P* v = m_layer->points_get();
					for (unsigned int i = 0; i < v->size(); ++i) {
						v->at(i)->show();
					}
					redraw_frame_polygons();
					return;
				}
			}
		}
		redraw_frame_polygons();
	} else if (w->m_remove_btn->isChecked()) {
		QList<QGraphicsItem *> v = items();
		for (int i = 0; i < v.size(); ++i) {
			QGraphicsItem* j = v.at(i);
			if (j->isUnderMouse()) {
				roto::db::point* item = dynamic_cast<
					roto::db::point*>(j);
				if (0 != item &&
						m_layer == item->get_owner_layer()) {
					m_layer->points_remove(item);
					delete item;
					m_layer->m_polygon_closed = false;
					redraw_frame_polygons();
					return;
				}
			}
		}
	} else {
		QPointF p = e->scenePos();
		if (! m_layer->points_get()->empty()) {
			QPointF f = m_layer->points_get()->at(0)->get_point_center();
			if ((p.x() < (f.x()+ 5) && p.x() >= (f.x() - 5)) &&
					(p.y() < (f.y()+ 5) && p.y() >= (f.y() - 5))) {
				m_layer->m_polygon_closed = true;
				redraw_frame_polygons();
				return;
			} 
		}
		if (! m_layer->is_image()) {
			roto::db::point* i = new roto::db::point(p);
			addItem(i);
			m_layer->points_add(i);
			redraw_frame_polygons();
		}
	}
}

void graphics_scene::
keyPressEvent(QKeyEvent* e)
{
	set_layer();
	roto::db::manager* manager = roto::db::manager::get();
	main_window* w = manager->get_main_window(); assert(0 != w);
	typedef roto::db::layer::points P;
	P* v = m_layer->points_get();
	qreal dx = 0;
	qreal dy = 0;
 	if (e->key() == Qt::Key_Up) {
		dy = -3;
        } else if(e->key() == Qt::Key_Down) {
		dy = 3;
        } else if(e->key() == Qt::Key_Left) {
		dx = -3;
        } else if(e->key() == Qt::Key_Right) {
		dx = 3;
        }
	if (w->m_select_btn->isChecked()) {
		if (-1 != m_selected_point_index) {// && 0 != m_layer->get_polygon_item()) {
			//QPolygonF* p = m_layer->get_polygon();
			//QPointF& k = const_cast<QPointF&>(p->at(m_selected_point_index));
			QPointF k = v->at(m_selected_point_index)->get_point_center();
			QPointF f(k.x() + dx, k.y() + dy);
			k.setX(k.x()+dx);
			k.setY(k.y()+dy);
			v->at(m_selected_point_index)->set_point_center(f);
			redraw_frame_polygons();
		}
		if (m_image_is_selected) {
			QPointF k = m_layer->get_image_center();
			QPointF f(k.x() + dx, k.y() + dy);
			m_layer->set_image_center(f);
			redraw_frame_polygons();
		}
		if (m_polygon_is_selected) {
			for (unsigned int i = 0; i < v->size(); ++i) {
				QPointF k = v->at(i)->get_point_center();
				QPointF f(k.x() + dx, k.y() + dy);
				v->at(i)->set_point_center(f);
			}
			redraw_frame_polygons();
		}
	}
}


void graphics_scene::
set_layer()
{
	assert(0 != m_frame);
	const roto::db::layer* q = m_frame->get_active_layer();
	m_layer = const_cast<roto::db::layer*>(q);
}

void graphics_scene::
set_frame(roto::db::frame* f)
{
	assert(0 != f);
	m_frame = f;
}

graphics_scene::
graphics_scene(QWidget*) 
	: QGraphicsScene()
	, m_layer(0)
	, itemToDraw(0)
	, m_selected_point_index(-1)
	, m_polygon_is_selected(false)
	, m_image_is_selected(false)
	, m_mouse_pos(QPointF(-1, -1))
	, m_mouse_is_released(true)
{
	draw_rectangle();
}

graphics_scene::
~graphics_scene()
{
}

