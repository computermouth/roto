#ifndef graphics_scene_H
#define graphics_scene_H

#include <QGraphicsScene>
#include <QMouseEvent>


// Forward declarations
namespace roto {
        namespace db {
                class layer;
                class frame;
        }
}


class graphics_scene : public QGraphicsScene
{
    Q_OBJECT

public:
    explicit graphics_scene(QWidget* parent = 0);
    ~graphics_scene();

	// public member functions
public:
	void set_layer();
	void set_frame(roto::db::frame*);

	void redraw_frame_polygons();

	void add_new_points_of_polygon();
	void remove_old_points_of_polygon();

	void draw_polygon();
	void draw_path();


	// private member functions
private:
	void draw_rectangle();
	void draw_line(QGraphicsSceneMouseEvent*);

	// member variables
private:
	roto::db::layer* m_layer;
	roto::db::frame* m_frame;
	QPolygonF* m_polygon;
    	QGraphicsLineItem* itemToDraw;
	int m_selected_point_index;
	bool m_polygon_is_selected;
	bool m_image_is_selected;
	QPointF m_mouse_pos;
	bool m_mouse_is_released;


private slots:
    void mousePressEvent(QGraphicsSceneMouseEvent*);
    void mouseMoveEvent(QGraphicsSceneMouseEvent*);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent*);
    void keyPressEvent(QKeyEvent*);

};

#endif // graphics_scene_H
