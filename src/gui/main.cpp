
// headers from this project
#include "main_window.h"

// headers from other projects
#include <db/manager.h>
#include <db/layer.h>
#include <db/frame.h>
#include <db/animation.h>

// headers from standard libraries
#include <QApplication>
#include <QResource>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QResource::registerResource("roto.rcc");
    main_window w;
    w.show();

    return app.exec();
}
