#ifndef ROTO_GUI_LIST_WIDGET_H
#define ROTO_GUI_LIST_WIDGET_H

#include "global.h"

#include "color_dialog.h"
#include <db/animation.h>
#include <db/layer.h>
#include <db/frame.h>

#include <QDebug>
#include <QGroupBox>
#include <QListWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QKeyEvent>
#include <QCheckBox>

#include <sstream>
#include <cassert>
#include <iostream>

class main_window;

Q_DECLARE_METATYPE(const roto::db::layer*)
Q_DECLARE_METATYPE(const roto::db::frame*)
Q_DECLARE_METATYPE(const roto::db::animation*)

class list_widget : public QGroupBox
{
Q_OBJECT

private:
	static int m_animation_counter;
	static int m_frame_counter;
	static int m_layer_counter;
	QLabel* m_index_label;
	QLabel* m_arrays_length_label;

private:
	global::type m_type;

public: 
	global::type get_type() { return m_type; }

private:
	QListWidget* m_list;

private:
	void connect_signals();
	void create_content();
	bool message_dialog(const std::string&, const std::string&, size_t);

public:
	QListWidget* get_list() const;
	void refresh(const roto::db::animation* a);
	void refresh(const roto::db::frame* f);
	void refresh();
	void clear();
	void remove_item(int);

private slots:
	void open_image_dialog();
	void open_new_dialog();
	void select_top();
	void select_down();
	void select_up();
	void select_bottom();
	void on_item_selection();

private:
	main_window* m_parent;
public:
	explicit list_widget(const QString& n, const global::type& t, main_window* parent = 0);
	~list_widget();
};

class list_checkbox : public QCheckBox
{
Q_OBJECT

private: 
	list_widget* m_list;
	int m_item_index;

private slots:
	void check_loop(bool b)
	{
		QListWidgetItem* item = m_list->get_list()->item(m_item_index);
		QVariant v;
		v = item->data(Qt::UserRole);
		const roto::db::animation* cl = v.value<const roto::db::animation*>();
		roto::db::animation* l = const_cast<roto::db::animation*>(cl);
		assert(0 != l);
		l->set_loop(b);
	}

public:
	void focusInEvent(QFocusEvent *e)
	{
		QListWidgetItem* item = m_list->get_list()->item(m_item_index);
		if (item == 0) {
			return;
		}
  		QCheckBox::focusInEvent(e);
		m_list->get_list()->setCurrentRow(m_item_index);
	}

	list_checkbox(list_widget* l, int i) 
		: QCheckBox()
		, m_list(l)
		, m_item_index(i)
	{
		connect(this, SIGNAL(clicked(bool)), this, SLOT(check_loop(bool)));
	}
};

class list_button : public QPushButton
{
Q_OBJECT

private: 
	list_widget* m_list;
	int m_item_index;

public:
	enum button_type {
		remove = 0,
		color,
		eject,
		max_type};

private slots:
	void remove_item() { m_list->remove_item(m_item_index);}

	void hide_item()
	{
		QListWidgetItem* item = m_list->get_list()->item(m_item_index);
		QVariant v;
		v = item->data(Qt::UserRole);
		const roto::db::layer* cl = v.value<const roto::db::layer*>();
		roto::db::layer* l = const_cast<roto::db::layer*>(cl);
		assert(0 != l);
		l->change_hide_state();
	}

	void choose_color()
	{
		QListWidgetItem* item = m_list->get_list()->item(m_item_index);
		QVariant v;
		v = item->data(Qt::UserRole);
		const roto::db::layer* cl = v.value<const roto::db::layer*>();
		roto::db::layer* l = const_cast<roto::db::layer*>(cl);
		assert(0 != l);
		color_dialog d;
		QColor c = d.get_color(l->get_color());
		l->set_color(c);
		this->setStyleSheet("QPushButton { background-color: "
				 + c.name() + "; }");
	}

public:
	void focusInEvent(QFocusEvent *e)
	{
		QListWidgetItem* item = m_list->get_list()->item(m_item_index);
		if (item == 0) {
			return;
		}
  		QPushButton::focusInEvent(e);
		m_list->get_list()->setCurrentRow(m_item_index);
	}

	list_button(list_widget* l, const button_type& t, int i) 
		: QPushButton()
		, m_list(l)
		, m_item_index(i)
	{
		if (t == button_type::remove) {
			connect(this, SIGNAL(clicked()), this, SLOT(remove_item()));
		} else if (t == button_type::color) {
			connect(this, SIGNAL(clicked()), this, SLOT(choose_color()));
		} else if (t == button_type::eject) {
			connect(this, SIGNAL(clicked()), this, SLOT(hide_item()));
		}
	}
};

class list_text : public QLineEdit
{
Q_OBJECT

private: 
	list_widget* m_list;
	int m_item_index;

public:
	enum text_type {
		name = 0,
		delay,
		max_type};

private:
	text_type m_type;

public:

	void focusOutEvent(QFocusEvent *e)
	{
		QListWidgetItem* item = m_list->get_list()->item(m_item_index);
		if (item == 0) {
  			QLineEdit::focusOutEvent(e);
			return;
		}
		QVariant v;
		v = item->data(Qt::UserRole);
		if (m_list->get_type() == global::animation) {
			const roto::db::animation* f = v.value<const roto::db::animation*>();
			roto::db::animation* l = const_cast<roto::db::animation*>(f);
			assert(0 != l);
			if (text_type::name == m_type) {
				l->set_name(text().toStdString());
			} 
		} else if (m_list->get_type() == global::frame)  {
			const roto::db::frame* f = v.value<const roto::db::frame*>();
			roto::db::frame* l = const_cast<roto::db::frame*>(f);
			assert(0 != l);
			if (text_type::name == m_type) {
				l->set_name(text().toStdString());
			} else if (text_type::delay == m_type) {
				std::stringstream s(text().toStdString());
				int i = 0;
				s >> i;
				l->set_delay(i);
			}
		} else if (m_list->get_type() == global::layer) {
			const roto::db::layer* f = v.value<const roto::db::layer*>();
			roto::db::layer* l = const_cast<roto::db::layer*>(f);
			assert(0 != l);
			if (text_type::name == m_type) {
				l->set_name(text().toStdString());
			}
		}
  		QLineEdit::focusOutEvent(e);
	}

	void focusInEvent(QFocusEvent *e)
	{
		QListWidgetItem* item = m_list->get_list()->item(m_item_index);
		if (item == 0) {
			return;
		}
  		QLineEdit::focusInEvent(e);
		m_list->get_list()->setCurrentRow(m_item_index);
	}

public:
	list_text(list_widget* l, const text_type& t, int i) 
		: QLineEdit()
		, m_list(l)
		, m_item_index(i)
		, m_type(t)
	{
	}
};

#endif // ROTO_GUI_LIST_WIDGET_H
