
#ifndef ROTO_ENGINE_CONF_SAVER
#define ROTO_ENGINE_CONF_SAVER

// headers from this project

// headers from other projects

// headers from standard libraries
#include <fstream>

// Forward declarations
namespace roto {
        namespace engine {
                class conf_saver;
        }
}

/**
 * This class stores the information related gui.
 */
class roto::engine::conf_saver 
{
private:
	std::string m_file_name;
public:
	void run();

	/// Special Member functions
public:
	/// Constructor
	conf_saver();
	/// Destructor
	~conf_saver();

private:
	conf_saver(const conf_saver&);
	conf_saver operator= (const conf_saver&);
};

#endif // ROTO_ENGINE_CONF_SAVER
