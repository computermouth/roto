
// headers from this project
#include <saver.h>

// headers from other projects
#include <db/animation.h>
#include <db/frame.h>
#include <db/layer.h>
#include <db/manager.h>
#include <db/point.h>

// headers from standard QT libraries
#include <QMessageBox>

// headers from standard libraries
#include <cassert>


void roto::engine::saver::
save_layer(std::ofstream& o, const roto::db::layer* l)
{
	assert(o.is_open()); assert(0 != l);
	o << "          - layer: " << l->get_name() << "\n";
	if (l->is_image()) {
		o << "            imgBase64: " << l->get_image_base() << "\n";
		o << "            x_image: "
					 << l->get_image_center().x() << "\n";
		o << "            y_image: "
					 << l->get_image_center().y() << "\n";
	} else {
		o << "            color: \""
			<< l->get_color().name().toStdString() << "\"\n";
		const roto::db::layer::points* p =
			l->points_get(); assert(0 != p);
		if (0 != p->size()) {
			roto::db::layer::points::const_iterator b = p->begin();
			o << "            x_coords:\n";
			for (; b != p->end(); ++b) {
				o << "              - " <<
				      (*b)->get_point_center().x() << std::endl;
			}
			b = p->begin();
			o << "            y_coords:\n";
			for (; b != p->end(); ++b) {
				o << "              - " <<
				      (*b)->get_point_center().y() << std::endl;
			}
		}
	}
}

void roto::engine::saver::
save_layers(std::ofstream& o, const roto::db::frame* f)
{
	assert(o.is_open()); assert(0 != f);
	const roto::db::frame::layers* l = f->layers_get(); assert(0 != l);
	roto::db::frame::layers::const_iterator b = l->begin();
	if (0 != l->size()) {
		o << "        layers:\n";
		for (; b != l->end(); ++b) {
			save_layer(o, *b);
		}
	}
}

void roto::engine::saver::
save_frame(std::ofstream& o, const roto::db::frame* f)
{
	assert(o.is_open()); assert(0 != f);
	o << "      - frame: " << f->get_name() << "\n";
	o << "        delay: " << f->get_delay() << "\n";
	save_layers(o, f);
}

void roto::engine::saver::
save_frames(std::ofstream& o, const roto::db::animation* a)
{
	assert(o.is_open()); assert(0 != a);
	const roto::db::animation::frames* f = a->frames_get(); assert(0 != f);
	roto::db::animation::frames::const_iterator b = f->begin();
	if (0 != f->size()) {
		o << "    frames:\n";
		for (; b != f->end(); ++b) {
			save_frame(o, *b);
		}
	}
}

void roto::engine::saver::
save_animation(std::ofstream& o, const roto::db::animation* a)
{
	assert(o.is_open()); assert(0 != a);
	o << "  - animation: " << a->get_name() << "\n";
	save_frames(o, a);
}

void roto::engine::saver::
save_animations(std::ofstream& o)
{
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	roto::db::manager::animations* a = m->animations_get();
	roto::db::manager::animations::const_iterator b = a->begin();
	if (0 != a->size()) {
		o << "\nanimations:\n";
		for (; b != a->end(); ++b) {
			save_animation(o, *b);
		}
	}
}

void roto::engine::saver::
message_dialog(const std::string& t, const std::string& m)
{
	QMessageBox msgBox;
	msgBox.setWindowTitle("Warning");
	msgBox.setText(t.c_str());
	msgBox.setInformativeText(m.c_str());
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.exec();
}

void roto::engine::saver::
check_unclosed_layers()
{
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	roto::db::manager::animations* a = m->animations_get();
	roto::db::manager::animations::const_iterator b = a->begin();
	std::string w;
	bool c = false;
	for (; b != a->end(); ++b) {
		const roto::db::animation::frames* f = (*b)->frames_get(); assert(0 != f);
		roto::db::animation::frames::const_iterator fr = f->begin();
		for (; fr != f->end(); ++fr) {
			const roto::db::frame::layers* l = (*fr)->layers_get(); assert(0 != l);
			roto::db::frame::layers::const_iterator ly = l->begin();
			for (; ly != l->end(); ++ly) {
				if (!(*ly)->m_polygon_closed && 0 != (*ly)->points_get()->size()) {
					w += (*b)->get_name() + " > " + (*fr)->get_name() + " > " + (*ly)->get_name() + "\n";
					c = true;
				}
			}
		}
	}
	if (c) {
		message_dialog("One or more layers are unfinished:", w);
	}
}

void roto::engine::saver::
run()
{
	check_unclosed_layers();
	assert(! m_file_name.empty());
	std::ofstream o(m_file_name.c_str()); assert(o.is_open());
	save_animations(o);
}

roto::engine::saver::
saver(const std::string& n)
	: m_file_name(n)
{
}

roto::engine::saver::
~saver()
{
}
