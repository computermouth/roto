
#ifndef ROTO_ENGINE_PLAYER
#define ROTO_ENGINE_PLAYER

// headers from this project

// headers from other projects

// headers from standard libraries
#include <QThread>

// Forward declarations
namespace roto {
        namespace engine {
                class player;
        }
        namespace db {
                class animation;
                class frame;
                class layer;
        }
}

/**
 * This class playes the animation content on scene
 */
class roto::engine::player : public QThread
{
public:
	void run();

	/// Special Member functions
public:
	/// Constructor
	player(double);
	/// Destructor
	~player();

private:
	double m_speed;

private:
	player(const player&);
	player operator= (const player&);
};

#endif // ROTO_ENGINE_PLAYER
