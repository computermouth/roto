
// headers from this project
#include <player.h>

// headers from other projects
#include <db/animation.h>
#include <db/manager.h>
#include <gui/main_window.h>
#include <gui/list_widget.h>

// headers from standard QT libraries

// headers from standard libraries
#include <cassert>
#include <unistd.h>


void roto::engine::player::
run()
{
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	main_window* w = m->get_main_window(); assert(0 != w);
	db::animation* a = w->get_selected_animation();
	if (0 != a) {
		db::animation::frames* f = a->frames_get();
		do {
			roto::db::animation::frames::const_iterator b = f->begin();
			for (int i = 0; b != f->end(); ++b, ++i) {
				assert(0 != *b);
				emit w->select_frames_row(i);
				usleep((*b)->get_delay() * 1000000 /
					       	(60 * m_speed));
			}
		} while(a->is_loop());
		emit w->stop_player();
	}
}

roto::engine::player::
player(double s)
	: QThread()
	, m_speed(s)
{
}

roto::engine::player::
~player()
{
}
