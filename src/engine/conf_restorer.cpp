
// headers from this project
#include <conf_restorer.h>

// headers from other projects
#include <db/manager.h>
#include <gui/main_window.h>
#include <gui/preferences_dialog.h>

// headers from standard QT libraries

// headers from standard libraries
#include <cassert>
#include <iostream>
#include <regex>

void roto::engine::conf_restorer::
read_window_settings(std::ifstream& i) 
{
	std::string r("");
	std::getline(i, r);
	r = std::regex_replace(r, std::regex("^ +"), "");
	std::string c = r.substr (0,1);
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	main_window* w = m->get_main_window(); assert(0 != w);
	while ("-" == c) {
		std::size_t p = r.find(" ");
		std::string n = r.substr(p+1);
		p = n.find(" ");
		std::string o = n.substr(0, p);
		std::string v = n.substr(p+1);
		QList<int> l = w->get_splitter()->sizes();
		assert(4 == l.count());
		if ("width:" == o) {
			std::stringstream s(v);
			int k = 0;
			s >> k;
			w->resize(k, w->height());
		} else if ("height:" == o) {
			std::stringstream s(v);
			int k = 0;
			s >> k;
			w->resize(w->width(), k);
		} else if ("animations-width:" == o) {
			std::stringstream s(v);
			int k = 0;
			s >> k;
			QList<int> sizes;
			sizes << k << l[1] << l[2] << l[3];
			w->get_splitter()->setSizes(sizes);
		} else if ("frames-width:" == o) {
			std::stringstream s(v);
			int k = 0;
			s >> k;
			QList<int> sizes;
			sizes << l[0] << k << l[2] << l[3];
			w->get_splitter()->setSizes(sizes);
		} else if ("layers-width:" == o) {
			std::stringstream s(v);
			int k = 0;
			s >> k;
			QList<int> sizes;
			sizes << l[0] << l[1] << k << l[3];
			w->get_splitter()->setSizes(sizes);
		} else if ("canvas-width:" == o) {
			std::stringstream s(v);
			int k = 0;
			s >> k;
			QList<int> sizes;
			sizes << l[0] << l[1] << l[2] << k;
			w->get_splitter()->setSizes(sizes);
			break;
		} else {
			break;
		}
		std::getline(i, r);
		r = std::regex_replace(r, std::regex("^ +"), "");
		c = r.substr (0,1);
	}
}

void roto::engine::conf_restorer::
read_canvas_settings(std::ifstream& i) 
{
	std::string r("");
	std::getline(i, r);
	r = std::regex_replace(r, std::regex("^ +"), "");
	std::string c = r.substr (0,1);
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	preferences_dialog* d = m->get_preferences_dialog(); assert(0 != d);
	while ("-" == c) {
		std::size_t p = r.find(" ");
		std::string n = r.substr(p+1);
		p = n.find(" ");
		std::string o = n.substr(0, p);
		std::string v = n.substr(p+1);
		if ("canvas-width:" == o) {
			std::stringstream s(v);
			int k = 0;
			s >> k;
			d->set_canvas_width(k);
		} else if ("canvas-height:" == o) {
			std::stringstream s(v);
			int k = 0;
			s >> k;
			d->set_canvas_height(k);
		} else if ("off-canvas-width:" == o) {
			std::stringstream s(v);
			int k = 0;
			s >> k;
			d->set_off_canvas_width(k);
		} else if ("off-canvas-height:" == o) {
			std::stringstream s(v);
			int k = 0;
			s >> k;
			d->set_off_canvas_height(k);
		} else {
			break;
		}
		std::getline(i, r);
		r = std::regex_replace(r, std::regex("^ +"), "");
		c = r.substr (0,1);
	}
}

void roto::engine::conf_restorer::
run()
{
	assert(! m_file_name.empty());
	std::ifstream i(m_file_name.c_str()); 
	if (! i.is_open()) {
		return;
	}
	while (! i.eof()) {
		std::string r("");
		std::getline(i, r);
		r = std::regex_replace(r, std::regex("^ +"), "");
		if (std::string::npos != r.find("window-settings:")) {
			read_window_settings(i);
		} else if (std::string::npos != r.find("canvas-settings:")) {
			read_canvas_settings(i);
		}
	}
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	main_window* w = m->get_main_window(); assert(0 != w);
	w->show();
}

roto::engine::conf_restorer::
conf_restorer()
	: m_file_name("")
{
	std::string homepath = getenv("HOME");
	m_file_name = homepath + "/.config/roto.conf";
}

roto::engine::conf_restorer::
~conf_restorer()
{
}
