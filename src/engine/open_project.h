
#ifndef ROTO_ENGINE_OPEN_PROJECT
#define ROTO_ENGINE_OPEN_PROJECT

// headers from this project

// headers from other projects
#include <db/manager.h>

// headers from standard libraries
#include <fstream>

// Forward declarations
namespace roto {
        namespace engine {
                class open_project;
        }
        namespace db {
                class animation;
                class frame;
                class layer;
                class manager;
        }
}

/**
 * This class stores the information related open_project.
 * Saver class responsible to save DB into yaml format
 */
class roto::engine::open_project 
{
private:
	std::string m_file_name;
	roto::db::animation* m_current_animation;
	roto::db::frame* m_current_frame;
	roto::db::layer* m_current_layer;
	bool m_x_enabled;
	bool m_y_enabled;
	uint64_t m_index;
private:
	void load_point(const std::string&);

	void load_layer(const std::string&);

	void load_frame(const std::string&);

	void load_animation(const std::string&);

	void refresh();
	void report_message(const std::string&);
	void create_object(std::ifstream&, const std::string&);
	void set_dalay_property(const std::string&);
	void set_color_property(const std::string&);
	void set_image_base(const std::string&);
	void set_x_property(const std::string&);
	void set_y_property(const std::string&);
	void load_point_y_coordinate(const std::string&);

public:
	void run();

	/// Special Member functions
public:
	/// Constructor
	open_project(const std::string&);
	/// Destructor
	~open_project();

private:
	open_project(const open_project&);
	open_project operator= (const open_project&);
};

#endif // ROTO_ENGINE_OPEN_PROJECT
