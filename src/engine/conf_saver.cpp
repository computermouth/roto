
// headers from this project
#include <conf_saver.h>

// headers from other projects
#include <db/manager.h>
#include <gui/main_window.h>
#include <gui/preferences_dialog.h>

// headers from standard QT libraries

// headers from standard libraries
#include <cassert>
#include <iostream>


void roto::engine::conf_saver::
run()
{
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	main_window* w = m->get_main_window(); assert(0 != w);
	preferences_dialog* p = m->get_preferences_dialog(); assert(0 != p);
	assert(! m_file_name.empty());
	std::ofstream o(m_file_name.c_str()); assert(o.is_open());
	o << "window-settings:\n";
	o << "        - width: " << w->width() << "\n";
	o << "        - height: " << w->height() << "\n";
	QSplitter* s = w->get_splitter();
	QList<int> l = s->sizes();
	assert(4 == l.count());
	o << "        - animations-width: " << l[0] << "\n";
	o << "        - frames-width: " << l[1] << "\n";
	o << "        - layers-width: " << l[2] << "\n";
	o << "        - canvas-width: " << l[3] << "\n";
	o << "canvas-settings:\n";
	o << "        - canvas-width: " << p->get_canvas_width() << "\n";
	o << "        - canvas-height: " << p->get_canvas_height() << "\n";
	o << "        - off-canvas-width: " << p->get_off_canvas_width() << "\n";
	o << "        - off-canvas-height: " << p->get_off_canvas_height() << "\n";
}

roto::engine::conf_saver::
conf_saver()
	: m_file_name("")
{
	std::string homepath = getenv("HOME");
	m_file_name = homepath + "/.config/roto.conf";
}

roto::engine::conf_saver::
~conf_saver()
{
}
