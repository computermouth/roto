
#ifndef ROTO_ENGINE_SAVER
#define ROTO_ENGINE_SAVER

// headers from this project

// headers from other projects

// headers from standard libraries
#include <fstream>

// Forward declarations
namespace roto {
        namespace engine {
                class saver;
        }
        namespace db {
                class animation;
                class frame;
                class layer;
        }
}

/**
 * This class stores the information related saver.
 * Saver class responsible to save DB into yaml format
 */
class roto::engine::saver 
{
private:
	std::string m_file_name;
private:
	void save_layer(std::ofstream&, const roto::db::layer*);
	void save_layers(std::ofstream&, const roto::db::frame*);

	void save_frame(std::ofstream&, const roto::db::frame*);
	void save_frames(std::ofstream&, const roto::db::animation*);

	void save_animation(std::ofstream&, const roto::db::animation*);
	void save_animations(std::ofstream&);
	void check_unclosed_layers();
	void message_dialog(const std::string& t, const std::string& m);
public:
	void run();

	/// Special Member functions
public:
	/// Constructor
	saver(const std::string&);
	/// Destructor
	~saver();

private:
	saver(const saver&);
	saver operator= (const saver&);
};

#endif // ROTO_ENGINE_SAVER
