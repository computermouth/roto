
#ifndef ROTO_ENGINE_CONF_RESTORER
#define ROTO_ENGINE_CONF_RESTORER

// headers from this project

// headers from other projects

// headers from standard libraries
#include <fstream>

// Forward declarations
namespace roto {
        namespace engine {
                class conf_restorer;
        }
}

/**
 * This class restores gui states from ~/roto.conf
 */
class roto::engine::conf_restorer 
{
private:
	std::string m_file_name;
	void read_window_settings(std::ifstream&);
	void read_canvas_settings(std::ifstream&);

public:
	void run();

	/// Special Member functions
public:
	/// Constructor
	conf_restorer();
	/// Destructor
	~conf_restorer();

private:
	conf_restorer operator= (const conf_restorer&);
};

#endif // ROTO_ENGINE_CONF_RESTORER
