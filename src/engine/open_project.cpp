
// headers from this project
#include <open_project.h>

// headers from other projects
#include <db/animation.h>
#include <db/frame.h>
#include <db/layer.h>
#include <db/point.h>
#include <gui/main_window.h>

// headers from standard QT libraries
#include <QMessageBox> 

// headers from standard libraries
#include <cassert>
#include <regex>


void roto::engine::open_project::
load_point_y_coordinate(const std::string& n)
{
	assert(0 != m_current_layer);
	db::layer::points* t = m_current_layer->points_get(); assert(0 != t);
	db::point* p = t->at(m_index); assert(0 != p);
	QPointF c = p->get_point_center();
	std::stringstream s(n);
	int y;
	s >> y;
	c.setY(y);
	p->set_point_center(c);
	++m_index;
}

void roto::engine::open_project::
load_point(const std::string& n)
{
	assert(0 != m_current_layer);
	db::point* p = m_current_layer->add_point();
	QPointF c = p->get_point_center();
	std::stringstream s(n);
	int x;
	s >> x;
	c.setX(x);
	p->set_point_center(c);
}


void roto::engine::open_project::
load_layer(const std::string& n)
{
	assert(0 != m_current_frame);
	m_current_layer = m_current_frame->add_layer(n);
	m_current_layer->m_polygon_closed = true;
}

void roto::engine::open_project::
load_frame(const std::string& n)
{
	assert(0 != m_current_animation);
	m_current_frame = m_current_animation->add_frame(n, 0);
}

void roto::engine::open_project::
load_animation(const std::string& n)
{
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	m_current_animation = m->add_animation(n);
}

void roto::engine::open_project::
create_object(std::ifstream&, const std::string& l)
{
	std::size_t p = l.find(" ");
	std::string n = l.substr(p+1);
	p = n.find(" ");
	std::string n1 = n.substr(0, p);
	std::string n2 = n.substr(p+1);
	if ("animation:" == n1) {
		load_animation(n2);
	} else if ("frame:" == n1) {
		load_frame(n2);
	} else if ("layer:" == n1) {
		load_layer(n2);
	} else if (m_x_enabled && !m_y_enabled) {
		load_point(n2);
		m_index = 0;
	} else if (! m_x_enabled && m_y_enabled) {
		load_point_y_coordinate(n2);
	}
}

void roto::engine::open_project::
set_dalay_property(const std::string& l)
{
	std::size_t p = l.find(" ");
	std::string n = l.substr(p+1);
	p = n.find(" ");
	std::string n1 = n.substr(0, p);
	std::string n2 = n.substr(p+1);
	assert(n1 == n2);
	std::stringstream s(n1);;
	uint64_t d;
	s >> d;
	m_current_frame->set_delay(d);
}

void roto::engine::open_project::
set_color_property(const std::string& l)
{
	std::size_t p = l.find(" ");
	std::string n = l.substr(p+1);
	p = n.find(" ");
	std::string n1 = n.substr(0, p);
	std::string n2 = n.substr(p+1);
	assert(n1 == n2);
	std::stringstream sr(n1.substr(2, 2));
	std::stringstream sg(n1.substr(4, 2));
	std::stringstream sb(n1.substr(6, 2));
	int r;
	int g;
	int b;
	sr >> std::hex >> r;
	sg >> std::hex >> g;
	sb >> std::hex >> b;
	QColor q;
	q.setRgb(r, g, b);
	m_current_layer->set_color(q);
}

void roto::engine::open_project::
set_image_base(const std::string& l)
{
	std::size_t p = l.find(" ");
	std::string n = l.substr(p+1);
	p = n.find(" ");
	std::string n1 = n.substr(0, p);
	std::string n2 = n.substr(p+1);
	assert(n1 == n2);
	m_current_layer->set_image_base(n1);
	m_current_layer->create_image_item();
}

void roto::engine::open_project::
set_x_property(const std::string& l)
{
	std::size_t p = l.find(" ");
	std::string n = l.substr(p+1);
	p = n.find(" ");
	std::string n1 = n.substr(0, p);
	std::string n2 = n.substr(p+1);
	assert(n1 == n2);
	std::stringstream sx(n1);
	int x;
	sx >> x;
	QPointF px;
	px.setX(x);
	m_current_layer->set_image_center(px);
}

void roto::engine::open_project::
set_y_property(const std::string& l)
{
	std::size_t p = l.find(" ");
	std::string n = l.substr(p+1);
	p = n.find(" ");
	std::string n1 = n.substr(0, p);
	std::string n2 = n.substr(p+1);
	assert(n1 == n2);
	std::stringstream sy(n1);
	int y;
	sy >> y;
	QPointF py = m_current_layer->get_image_center();
	py.setY(y);
	m_current_layer->set_image_center(py);
}

void roto::engine::open_project::
run()
{
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	assert(! m_file_name.empty());
	std::ifstream i(m_file_name.c_str()); assert(i.is_open());
	while (! i.eof()) {
		std::string r("");
		std::getline(i, r);
		r = std::regex_replace(r, std::regex("^ +"), "");
		std::string c = r.substr (0,1);
		if ("-" == c) {
			create_object(i, r);
		} else if ("d" == c) {
			set_dalay_property(r);
		} else if ("c" == c) {
			set_color_property(r);
		} else if ("i" == c) {
			set_image_base(r);
		} else if ("x" == c) {
			if (m_current_layer->is_image()) {
				set_x_property(r);
			} else {
				m_x_enabled = true;
				m_y_enabled = false;
			}
		} else if ("y" == c) {
			if (m_current_layer->is_image()) {
				set_y_property(r);
			} else {
				m_y_enabled = true;
				m_x_enabled = false;
			}
		} else if ("" == c || "a" == c || "f" == c || "l" == c) {
			continue;
		} else {
			report_message("Not supported YAML file.");
			break;
		}
	}
	refresh();
}

void roto::engine::open_project::
refresh()
{
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	main_window* w = m->get_main_window(); assert(0 != w);
	w->refresh();
}

void roto::engine::open_project::
report_message(const std::string& s)
{
	QMessageBox msgBox;
	msgBox.setText(s.c_str());
	msgBox.exec();
}


roto::engine::open_project::
open_project(const std::string& n)
	: m_file_name(n)
	, m_current_animation(0)
	, m_current_frame(0)
	, m_current_layer(0)
	, m_x_enabled(false)
	, m_y_enabled(false)
	, m_index(0)
{
}

roto::engine::open_project::
~open_project()
{
}
