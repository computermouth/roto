
// headers from this project
#include <animation.h>
#include <frame.h>

// headers from other projects

// headers from standard libraries
#include <algorithm>
#include <cassert>
#include <iostream>

const roto::db::animation::frames*
roto::db::animation::
frames_get() const
{
	return &m_frames;
}

roto::db::animation::frames*
roto::db::animation::
frames_get() 
{
	return &m_frames;
}

roto::db::frame*
roto::db::animation::
add_frame(db::frame* f)
{
	assert(0 != f);
	m_frames.push_back(f);
	return f;
}

roto::db::frame*
roto::db::animation::
add_frame(const std::string& l, int d)
{
	assert(! l.empty());
	db::frame* f = new roto::db::frame(l, d);
	m_frames.push_back(f);
	return f;
}

struct is_equal {
private:
	std::string m_name;
public:
    	is_equal(const std::string& n) 
		: m_name(n) { }

	bool operator() (const roto::db::frame* f) { 
		assert(0 != f);
		return f->get_name() == m_name; 
	}
};

void roto::db::animation::
remove_frame(const std::string& l)
{
	frames::iterator i = std::find_if(m_frames.begin(),
						m_frames.end(), is_equal(l));
	if (m_frames.end() != i) {
		assert(0 != *i);
		delete *i;
		m_frames.erase(i);
	}
}

std::string roto::db::animation::
get_name() const
{
	return m_name;
}

void roto::db::animation::
set_name(const std::string& n)
{
	assert(!n.empty());
	m_name = n;
}

bool roto::db::animation::
is_loop() const
{
	return m_play_loop;
}

void roto::db::animation::
set_loop(bool b)
{
	m_play_loop = b;
}

roto::db::animation::
animation(const std::string& n)
	: m_frames()
	, m_name(n)
	, m_play_loop(false)
{
	assert(!m_name.empty());
}

roto::db::animation::
animation(const roto::db::animation& a)
	: m_frames()
	, m_name(a.get_name())
	, m_play_loop(a.is_loop())
{
	frames::const_iterator b = a.frames_get()->begin();
	for (; b != a.frames_get()->end(); ++b) {
		assert(0 != *b);
		m_frames.push_back(new roto::db::frame(**b));
	}
}

roto::db::animation::
~animation()
{
	frames::iterator b = m_frames.begin();
	for (; b != m_frames.end(); ++b) {
		assert(0 != *b);
		delete *b;
	}
	m_frames.clear();
}


