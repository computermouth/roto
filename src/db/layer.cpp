
// headers from this layer
#include <frame.h>
#include <layer.h>
#include <point.h>

// headers from other layers
#include <gui/graphics_scene.h>

// headers from standard QT libraries
#include <QByteArray>
#include <QBuffer>

// headers from standard libraries

#include <cassert>
#include <QLabel>

void roto::db::layer::
set_image_center(QPointF& c)
{
        m_image_center = c;
	m_image_item->setX(m_image_center.x());
	m_image_item->setY(m_image_center.y());
}

//size_t roto::db::layer::
//get_point_diff(QPointF& c) const
//{
//	size_t x = std::abs(m_image_center.x() - c.x());
//	size_t y = std::abs(m_image_center.y() - c.y());
//	if (x > y) {
//		return x;
//	}
//	return y;
//}

QPointF roto::db::layer::
get_image_center() const
{
        return m_image_center;
}

bool roto::db::layer::
is_image() const
{
        return ! m_image_path.empty() || !m_image_base.empty();
}

void roto::db::layer::
set_image_base(std::string f)
{
	assert(! f.empty());
	m_image_base = f;
}

void roto::db::layer::
set_image_path(std::string f)
{
	assert(! f.empty());
	m_image_path = f;

}

std::string roto::db::layer::
get_image_path() const
{
        return m_image_path;
}
 
std::string roto::db::layer::
get_image_base() const
{
        return m_image_base;
}
 
void roto::db::layer::
clear()
{
	delete m_image_item;
	m_image_item = 0;
}

void roto::db::layer::
create_image_item()
{
	if (0 == m_qimage) {
		m_qimage = new QImage();
		if (!m_image_base.empty()) {
			m_qimage->loadFromData(
			   QByteArray::fromBase64(m_image_base.c_str()), "PNG");
		} else {
			m_qimage->load(m_image_path.c_str());
			QByteArray ba;
			QBuffer bu(&ba);
			m_qimage->save(&bu, "PNG");
			m_image_base = ba.toBase64().toStdString();
		}
	}
	assert(! m_qimage->isNull());
	QPixmap image = QPixmap::fromImage(*m_qimage);
	m_image_item = new QGraphicsPixmapItem(image);
	if (is_active()) {
		m_image_item->setFlag(QGraphicsItem::ItemIsMovable, true);
	} else {
		m_image_item->setFlag(QGraphicsItem::ItemIsMovable, false);
	}
	m_image_item->setX(m_image_center.x());
	m_image_item->setY(m_image_center.y());
}
 
QGraphicsPixmapItem* roto::db::layer::
get_image_item() const
{
        return m_image_item;
}
 
void roto::db::layer::
set_owner_frame(roto::db::frame* f)
{
	assert(0 != f);
        m_owner_frame = f;
}

roto::db::frame*
roto::db::layer::
get_owner_frame() const
{
        return m_owner_frame;
}

roto::db::layer::points*
roto::db::layer::
points_get()
{
        return &m_points;
}

const roto::db::layer::points*
roto::db::layer::
points_get() const
{
        return &m_points;
}

roto::db::point*
roto::db::layer::
add_point()
{
	db::point* f = new roto::db::point();
	f->set_owner_layer(this);
	if (m_points.empty()) {
		f->set_color("blue");
	}
	m_points.push_back(f);
	return f;
}

void roto::db::layer::
points_add(point* i)
{
        assert(0 != i);
	if (m_points.empty()) {
		i->set_color("blue");
	}
        m_points.push_back(i);
        i->set_owner_layer(this);
}

void roto::db::layer::
points_remove(const point* const i)
{
        assert(0 != i);
        points::iterator t = std::find(
		m_points.begin(), m_points.end(), i);
        if (t != m_points.end()) {
                m_points.erase(t);
        }
}

void roto::db::layer::
set_polygon_item(QGraphicsPolygonItem* p)
{
        assert(0 != p);
        m_polygon_item = p;
	m_line_item = 0;
}

void roto::db::layer::
set_line_item(QGraphicsPathItem* p)
{
        assert(0 != p);
        m_line_item = p;
	m_polygon_item = 0;
}

QGraphicsPolygonItem* roto::db::layer::
get_polygon_item()
{
        return m_polygon_item;
}

QGraphicsPolygonItem* roto::db::layer::
get_polygon_item() const
{
        return m_polygon_item;
}

QGraphicsPathItem* roto::db::layer::
get_line_item()
{
        return m_line_item;
}

QGraphicsPathItem* roto::db::layer::
get_line_item() const
{
        return m_line_item;
}

void roto::db::layer::
set_polygon(QPolygonF* p)
{
        assert(0 != p);
        m_polygon = p;
}

QPolygonF* roto::db::layer::
get_polygon()
{
	assert(0 != m_polygon);
        return m_polygon;
}

QPolygonF* roto::db::layer::
get_polygon() const
{
	assert(0 != m_polygon);
        return m_polygon;
}

void roto::db::layer::
set_color(QColor c)
{
        assert(c.isValid());
        m_color = c;
	graphics_scene* g = m_owner_frame->get_graphics_scene(); assert(0 != g);
	g->redraw_frame_polygons();

}

QColor roto::db::layer::
get_color()
{
        return m_color;
}

QColor roto::db::layer::
get_color() const
{
        return m_color;
}

void roto::db::layer::
set_name(const std::string& c)
{
        assert(! c.empty());
        m_name = c;
}

std::string roto::db::layer::
get_name() const
{
        return m_name;
}

bool roto::db::layer::
is_active() const
{
	return m_is_active;
}

void roto::db::layer::
set_active()
{
	if (is_image()) {
		m_image_item->setFlag(QGraphicsItem::ItemIsMovable, true);
	} else {
		points::iterator i = m_points.begin();
		for(; i != m_points.end(); ++i) {
			(*i)->setFlag(QGraphicsItem::ItemIsMovable, true);
			(*i)->set_color("black");
		}
	}
	m_is_active = true;
}

void roto::db::layer::
set_inactive()
{
	if (is_image()) {
		m_image_item->setFlag(QGraphicsItem::ItemIsMovable, false);
	} else {
		points::iterator i = m_points.begin();
		for(; i != m_points.end(); ++i) {
			(*i)->setFlag(QGraphicsItem::ItemIsMovable, false);
			(*i)->set_color("gray");
		}
	}
	m_is_active = false;
}

void roto::db::layer::
change_hide_state()
{
	m_is_hided = !m_is_hided;
        points::iterator i = m_points.begin();
	if (is_image()) {
		if (m_is_hided) {
			m_image_item->hide();
		} else {
			m_image_item->show();
		}
		return;

	}
	for(; i != m_points.end(); ++i) {
		if (m_is_hided) {
			(*i)->hide();
		} else {
			(*i)->show();
		}
	}
	if (m_is_hided) {
		hide();
	} else {
		show();
	}
}

void roto::db::layer::
hide()
{
	if (0 != m_polygon_item) {
		m_polygon_item->hide();
	} else {
		m_line_item->hide();
	}	
}

void roto::db::layer::
show()
{
	if (0 != m_polygon_item) {
		m_polygon_item->show();
	} else {
		m_line_item->show();
	}	
}

bool roto::db::layer::
is_hided() const
{
	return m_is_hided;
}

void roto::db::layer::
set_image(QLabel* i) 
{
	m_image = i;
}

QLabel* roto::db::layer::
get_image() const
{
	return m_image;
}

roto::db::layer::
layer(const roto::db::layer& n)
	: m_points()
	, m_is_active(n.is_active())
	, m_image_center(n.get_image_center())
	, m_owner_frame(n.get_owner_frame())
	, m_image_path(n.get_image_path())
	, m_image_base(n.get_image_base())
	, m_image_item(0)
	, m_qimage(0)
	, m_polygon_item(n.get_polygon_item())
	, m_line_item(n.get_line_item())
	, m_polygon(0)
	, m_color(n.get_color())
	, m_name(n.get_name())
	, m_is_hided(n.is_hided())
	, m_image()
	, m_polygon_closed(n.m_polygon_closed)
{
	m_polygon = new QPolygonF();
	m_image = new QLabel();
	points::const_iterator b = n.points_get()->begin();
	for (; b != n.points_get()->end(); ++b) {
		assert(0 != *b);
		db::point* f = new roto::db::point(**b);
		f->set_owner_layer(this);
		m_points.push_back(f);
	}
	if (n.is_image()) {
		create_image_item();
	}
}

roto::db::layer::
layer(const std::string& n)
	: m_points()
	, m_is_active(true)
	, m_image_center()
	, m_owner_frame(0)
	, m_image_path("")
	, m_image_base("")
	, m_image_item(0)
	, m_qimage(0)
	, m_polygon_item(0)
	, m_line_item(0)
	, m_polygon(0)
	, m_color(QColor("#FF0000"))
	, m_name(n)
	, m_is_hided(false)
	, m_image(0)
	, m_polygon_closed(false)
{
	m_polygon = new QPolygonF();
	m_image = new QLabel();
}

roto::db::layer::
~layer()
{
	if (0 != m_polygon) {
		delete m_polygon;
		m_polygon = 0;
	}
	if (0 != m_image) {
		delete m_image;
		m_image = 0;
	}
	if (0 != m_qimage) {
		delete m_qimage;
		m_qimage = 0;
	}
	if (0 != m_image_item) {
		delete m_image_item;
		m_image_item = 0;
	}
}
