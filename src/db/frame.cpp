
// headers from this project
#include <frame.h>
#include <manager.h>

// headers from other projects
#include <gui/graphics_scene.h>
#include <gui/main_window.h>
#include <gui/preferences_dialog.h>

// headers from standard libraries
#include <cassert>

void roto::db::frame::
set_active_layer(const roto::db::layer* l)
{
	assert(0 != l);
	m_active_layer = l;
	typedef roto::db::frame::layers L;
	L* s = layers_get();
	L::iterator i = s->begin();
	for(; i != s->end(); ++i) {
		assert(0 != *i);
		roto::db::layer* r = const_cast<roto::db::layer*>(*i);
		if (l == r) {
			r->set_active();
		} else {
			r->set_inactive();
		}
	}
}

const roto::db::layer*
roto::db::frame::
get_active_layer() const
{
	if (0 == m_layers.size()) {
		return 0;
	}
	return m_active_layer;
}

graphics_scene*
roto::db::frame::
get_graphics_scene() const
{
	assert(0 != m_graphics_scene);
	return m_graphics_scene;
}

graphics_scene*
roto::db::frame::
get_graphics_scene()
{
	assert(0 != m_graphics_scene);
	return m_graphics_scene;
}

roto::db::frame::layers*
roto::db::frame::
layers_get() 
{
	return &m_layers;
}

const roto::db::frame::layers*
roto::db::frame::
layers_get() const
{
	return &m_layers;
}

void roto::db::frame::
add_layer(const layer* l)
{
	assert(0 != l);
	m_layers.push_back(l);
}

struct is_equal {
private:
	std::string m_name;
public:
    	is_equal(const std::string& n) 
		: m_name(n) { }

	bool operator() (const roto::db::layer* f) { 
		assert(0 != f);
		return f->get_name() == m_name; 
	}
};

void roto::db::frame::
remove_layer(const std::string& l)
{
	layers::iterator i = std::find_if(m_layers.begin(), m_layers.end(), is_equal(l));
	if (m_layers.end() != i) {
		assert(0 != *i);
		delete *i;
		m_layers.erase(i);
	}
}

std::string roto::db::frame::
get_name() const
{
	return m_name;
}

void roto::db::frame::set_name(const std::string& n)
{
	assert(!n.empty());
	m_name = n;
}

uint64_t roto::db::frame::
get_delay() const
{
	return m_delay;
}

void roto::db::frame::set_delay(uint64_t d)
{
	m_delay = d;
}

roto::db::layer*
roto::db::frame::
add_layer(const std::string& l)
{
	assert(! l.empty());
	db::layer* f = new roto::db::layer(l);
	f->set_owner_frame(this);
	m_layers.push_back(f);
	return f;
}

roto::db::frame::
frame(const roto::db::frame& f)
	: m_name(f.get_name())
	, m_delay(f.get_delay())
	, m_active_layer(0)
{
	assert(! m_name.empty());
	layers::const_iterator b = f.layers_get()->begin();
	for (; b != f.layers_get()->end(); ++b) {
		assert(0 != *b);
		db::layer* f = new roto::db::layer(**b);
		f->set_owner_frame(this);
		m_layers.push_back(f);
	}
	m_graphics_scene = new graphics_scene();
	m_graphics_scene->set_frame(this);
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	main_window* w = m->get_main_window(); assert(0 != w);
	QGraphicsView* v = w->get_graphics_view(); assert(0 != v);
	preferences_dialog* p = m->get_preferences_dialog();
	// Add checker here
	int x = (p->get_off_canvas_width() - p->get_canvas_width())/2;
	int y = (p->get_off_canvas_height() - p->get_canvas_height())/2;
        m_graphics_scene->setSceneRect(-x, -y,
		p->get_off_canvas_width(), p->get_off_canvas_height());
	v->setScene(m_graphics_scene);

}

roto::db::frame::
frame(const std::string& n, uint64_t d)
	: m_name(n)
	, m_delay(d)
	, m_active_layer(0)
{
	assert(! m_name.empty());
	m_graphics_scene = new graphics_scene();
	m_graphics_scene->set_frame(this);
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	main_window* w = m->get_main_window(); assert(0 != w);
	QGraphicsView* v = w->get_graphics_view(); assert(0 != v);
	preferences_dialog* p = m->get_preferences_dialog();
	int x = (p->get_off_canvas_width() - p->get_canvas_width())/2;
	int y = (p->get_off_canvas_height() - p->get_canvas_height())/2;
        m_graphics_scene->setSceneRect(-x, -y,
		p->get_off_canvas_width(), p->get_off_canvas_height());
	v->setScene(m_graphics_scene);
}

roto::db::frame::
~frame()
{
	layers::iterator b = m_layers.begin();
	for (; b != m_layers.end(); ++b) {
		assert(0 != *b);
		delete *b;
	}
	m_layers.clear();
	assert(0 != m_graphics_scene);
	delete m_graphics_scene;
}

