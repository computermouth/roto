
#ifndef ROTO_DB_ANIMATION_H
#define ROTO_DB_ANIMATION_H

// headers from this project

// headers from other projects

// headers from standard libraries
#include <string>
#include <vector>

// Forward declarations
namespace roto {
        namespace db {
                class animation;
                class frame;
        }
}

/**
 * This class stores the information related animation.
 * Animations is a collection of frames and has unique name.
 */
class roto::db::animation 
{
	/// Collection of frames
public:
	typedef std::vector<const frame*> frames;

	/// @return The collection of frames
	const frames* frames_get() const;
	frames* frames_get();

        /// Adds specified frame to the collection
	roto::db::frame* add_frame(db::frame*);
	roto::db::frame* add_frame(const std::string& l, int d);
	/// Removes the frame with the given name from collection
	void remove_frame(const std::string& l);

private:
	frames m_frames;

	/// The name of the animation
public:
	/// @return The name of the animation
	std::string get_name() const;
	/// Sets the specified value as a name
	void set_name(const std::string& n);
private:
	std::string m_name;

	/// The play loop of the animation
public:
	/// @return The play loop of the animation
	bool is_loop() const;
	void set_loop(bool n);
private:
	bool m_play_loop;

	/// Special Member functions
public:
	/// Constructor
	animation(const std::string& n);
	animation(const animation&);
	/// Destructor
	~animation();

private:
	animation();
	animation operator= (const animation&);
};

#endif // ROTO_DB_ANIMATION_H
