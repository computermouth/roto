
// headers from this project
#include <point.h>
#include <layer.h>
#include <manager.h>

// headers from other projects

// headers from QT libraries
#include <QPainter>

// headers from standard libraries
#include <cassert>

void roto::db::point::
set_owner_layer(roto::db::layer* l)
{
        m_owner_layer = l;
}

roto::db::layer*
roto::db::point::
get_owner_layer() const
{
        return m_owner_layer;
}

void roto::db::point::
set_point_center(QPointF& c)
{
        m_point_center = c;
}

//size_t roto::db::point::
//get_point_diff(QPointF& c) const
//{
//	size_t x = std::abs(m_point_center.x() - c.x());
//	size_t y = std::abs(m_point_center.y() - c.y());
//	if (x > y) {
//		return x;
//	}
//	return y;
//}

QPointF roto::db::point::
get_point_center() const
{
        return m_point_center;
}

void roto::db::point::
set_color(const std::string& c)
{
        assert(! c.empty());
        m_color = c;
	if ("black" == c) {
		QColor bra(Qt::black);
		setPen(bra);
	} else if ("green" == c) {
		QColor bra(Qt::green);
		setPen(bra);
	} else if ("gray" == c) {
		QColor bra(Qt::gray);
		setPen(bra);
	} else if ("blue" == c) {
		QColor bra(Qt::blue);
		setPen(bra);
	} else if ("red" == c) {
		QColor bra(Qt::red);
		setPen(bra);
	} else {
		QColor bra(Qt::black);
		setPen(bra);
	}
}

std::string roto::db::point::
get_color() const
{
        return m_color;
}

roto::db::point::
point()
{
}

roto::db::point::
point(const point& p)
	: QGraphicsEllipseItem()
	, m_color(p.get_color())
	, m_owner_layer()
	, m_point_center(p.get_point_center())
{
}

roto::db::point::
point(QPointF& p)
{
	m_point_center = p;
	roto::db::manager* m = roto::db::manager::get(); assert(0 != m);
	double f = m->get_zoom_factor();
	setRect(p.x() - 5 / f, p.y() - 5 / f, 10 / f, 10 / f);
	setFlag(QGraphicsItem::ItemIsMovable, true);
	QPen pen(Qt::black);
	pen.setWidthF(1/f);
	setPen(pen);
}

roto::db::point::
~point()
{
}

