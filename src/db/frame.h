
#ifndef ROTO_DB_FRAME_H
#define ROTO_DB_FRAME_H

// headers from this project

// headers from other projects

// headers from standard libraries
#include <vector>
#include <string>
#include <stdint.h>

// Forward declarations
namespace roto {
        namespace db {
                class frame;
                class layer;
        }
}

class graphics_scene;

/// This class stores information related to frame, its name and a delay value which specifies how long the frame will be shown during animation play. It also stores collection of the layers from which the frame is constructed.
class roto::db::frame 
{
	/// Collection of layers
public:
	typedef std::vector<const layer*> layers;

	/// @return The collection of the layers
	const layers* layers_get() const;
	layers* layers_get();

	roto::db::layer* add_layer(const std::string& l);

	/// Adds the specified layer to the collection
	void add_layer(const layer* l);

	/// Removes the layer with specified name from the collection
	void remove_layer(const std::string& n);

private:
	layers m_layers;

	/// The graphics_scene member
private:
	graphics_scene* m_graphics_scene;

public:
	/// @return The graphics_scene
	graphics_scene* get_graphics_scene() const;

	graphics_scene* get_graphics_scene();

	/// The name of the frame
public:
	/// @return The name of the frame
	std::string get_name() const;
	/// Sets the specified value as a name
	void set_name(const std::string& n);
private:
	std::string m_name;

	/// The delay of the frame
public:
	/// @return The delay of the frame
	uint64_t get_delay() const;
	/// Sets the specified value as a delay 
	void set_delay(uint64_t d);
private:
	uint64_t m_delay;

	/// The active layer
public:
	/// @return The active layer
	const roto::db::layer* get_active_layer() const;

	/// Sets the active layer
	void set_active_layer(const roto::db::layer*);
private:
	const roto::db::layer* m_active_layer;

	/// Special member functions
public:
	/// Constructor
	frame(const std::string& n, uint64_t d);
	frame(const frame&);
	/// Destructor
	~frame();

private:
	frame();
	frame operator= (const frame&);
};

#endif // ROTO_DB_FRAME_H
