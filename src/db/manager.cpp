
// headers from this project
#include "manager.h"
#include "animation.h"

// headers from other projects

// headers from standard libraries
#include <cassert>
#include <cstddef>
#include <algorithm>

roto::db::manager* roto::db::manager::m_manager = NULL;

roto::db::manager* roto::db::manager::
get()
{
	if (NULL == m_manager) {
		m_manager = new manager();
	}
	return m_manager;
}

roto::db::manager::
manager()
	: m_main_window(0)
	, m_preferences(0)
	, m_zoom_factor(1)
	, m_animations()
{}

roto::db::manager::
~manager()
{}

double roto::db::manager::
get_zoom_factor() 
{
	return m_zoom_factor;
}

void roto::db::manager::
set_zoom_factor(double w) 
{
	m_zoom_factor = w;

}

preferences_dialog* roto::db::manager::
get_preferences_dialog() 
{
	assert(0 != m_preferences);
	return m_preferences;
}

void roto::db::manager::
set_preferences_dialog(preferences_dialog* w) 
{
	assert(0 == m_preferences);
	m_preferences = w;

}

main_window* roto::db::manager::
get_main_window() 
{
	assert(0 != m_main_window);
	return m_main_window;
}

void roto::db::manager::
set_main_window(main_window* w) 
{
	assert(0 == m_main_window);
	m_main_window = w;
}

roto::db::manager::animations* roto::db::manager::
animations_get() 
{
	return &m_animations;
}

roto::db::animation*
roto::db::manager::
add_animation(const std::string& l)
{
	assert(! l.empty());
	db::animation* a = new roto::db::animation(l);
	m_animations.push_back(a);
	return a;
}

void roto::db::manager::
add_animation(const roto::db::animation* a)
{
	assert(0 != a);
	m_animations.push_back(a);
}

struct is_equal {
private:
	std::string m_name;
public:
    	is_equal(const std::string& n) 
		: m_name(n) { }

	bool operator() (const roto::db::animation* a) { 
		assert(0 != a);
		return a->get_name() == m_name; 
	}
};

void roto::db::manager::
remove_animation(const std::string& l)
{
	animations::iterator i = std::find_if(m_animations.begin(), m_animations.end(), is_equal(l));
	if (m_animations.end() != i) {
		assert(0 != *i);
		delete *i;
		m_animations.erase(i);
	}
}

