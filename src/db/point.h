
#ifndef ROTO_DB_POINT_H
#define ROTO_DB_POINT_H

// headers from this project

// headers from other projects

// headers from QT
#include <QGraphicsEllipseItem>
#include <QDebug>


// headers from standard libraries
#include <string>

// Forward declarations
namespace roto {
        namespace db {
                class point;
                class layer;
        }
}

class roto::db::point : public QGraphicsEllipseItem
{
        //member color
public:
	void set_color(const std::string&);

	std::string get_color() const;

private:
	std::string m_color;

        //member owner layer
public:
	void set_owner_layer(layer*);

	layer* get_owner_layer() const;

private:
	layer* m_owner_layer;

        //member point center
public:
	void set_point_center(QPointF&);

	//size_t get_point_diff(QPointF& c) const;

	QPointF get_point_center() const;

private:
	QPointF m_point_center;

public:
        point();

        point(QPointF&);

        point(const point&);

        ~point();

private:
        point operator= (const point&);
};

#endif // ROTO_DB_POINT_H
