
#ifndef ROTO_DB_MANAGER_H
#define ROTO_DB_MANAGER_H

#include <vector>
#include <string>

namespace roto {
	namespace db {
		class manager;
		class animation;
	}
}

class main_window;
class preferences_dialog;

/// This is a single-ton object which stores the animations collection and provides functionality to access and modify them
class roto::db::manager 
{
	/// Single-ton API 
public:
	static manager* get();
private:
	static manager* m_manager;

	/// Animations collection
public:
	typedef std::vector<const animation*> animations;
	/// @return The collection of the animations
	animations* animations_get();

	preferences_dialog* get_preferences_dialog();

	void set_preferences_dialog(preferences_dialog*);

	double get_zoom_factor();

	void set_zoom_factor(double w);

	main_window* get_main_window();

	void set_main_window(main_window*);

  	/// Adds specified animation to the collection
	db::animation* add_animation(const std::string& l);
	void add_animation(const roto::db::animation* a);
	void remove_animation(const std::string& l);

private:
	main_window* m_main_window;
	preferences_dialog* m_preferences;
	double m_zoom_factor;


	animations m_animations;


	/// Special member functions
private:
	manager();
	manager(const manager&);
	manager operator= (const manager&);
	~manager();
};

#endif // ROTO_DB_MANAGER_H

