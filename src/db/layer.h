
#ifndef ROTO_DB_LAYER_H
#define ROTO_DB_LAYER_H

// headers from this project

// headers from other projects

// headers from QT layers
#include <QColor>
#include <QPolygonF>
#include <QGraphicsPolygonItem>
#include <QGraphicsPathItem>
#include <QLabel>


// headers from standard libraries
#include <string>
#include <vector>

// Forward declarations
namespace roto {
        namespace db {
                class frame;
                class layer;
                class point;
        }
}

class roto::db::layer 
{
	/// @name points
public:
        typedef std::vector<point*> points;

        /// @brief Gets points
        points* points_get();

	roto::db::point* add_point();

        const points* points_get() const;

        /**
         * @brief Adds points to the collection
         *
         * @param i - the point that should be added
         *
         * @pre 0 != i
         *
         */
        void points_add(point* i);

        /**
         * @brief Removes point from the collection
         *
         * @param i - existing point that should be removed
         *
         * @pre 0 != i
         */
        void points_remove(const point* const i);

private:
        points m_points;
        bool m_is_active;

        //member point center
public:
	void set_image_center(QPointF&);

	//size_t get_point_diff(QPointF& c) const;

	QPointF get_image_center() const;

private:
	QPointF m_image_center;

        //member image path
public:
	bool is_image() const;

        void set_image_base(std::string);

        void set_image_path(std::string);

	std::string get_image_path() const;

	std::string get_image_base() const;

        //member image item
public:
        void clear();

        void create_image_item();

	QGraphicsPixmapItem* get_image_item() const;

        //member owner frame
public:
        void set_owner_frame(roto::db::frame*);

        roto::db::frame* get_owner_frame() const;

        bool is_active() const;

private:
        roto::db::frame* m_owner_frame;

        std::string m_image_path;

        std::string m_image_base;

	QGraphicsPixmapItem* m_image_item;

	QImage* m_qimage;

        //member polygon item
public:
        void set_polygon_item(QGraphicsPolygonItem*);

        QGraphicsPolygonItem* get_polygon_item() const;

        QGraphicsPolygonItem* get_polygon_item();

        void set_line_item(QGraphicsPathItem*);

        QGraphicsPathItem* get_line_item() const;

        QGraphicsPathItem* get_line_item();

private:
	QGraphicsPolygonItem* m_polygon_item;

	QGraphicsPathItem* m_line_item;

        //member polygon
public:
        void set_polygon(QPolygonF*);

        QPolygonF* get_polygon() const;

        QPolygonF* get_polygon();

private:
        QPolygonF* m_polygon;

        //member color
public:
        void set_color(QColor);

        QColor get_color() const;

        QColor get_color();

private:

        QColor m_color;

        //member name
public:
        void set_name(const std::string&);

        std::string get_name() const;

private:
        std::string m_name;

public:
	void set_active();
	void set_inactive();
	void change_hide_state();
	bool is_hided() const;
	void hide();
	void show();

	// private member items
private:
	bool m_is_hided;
	
        //member image
public:
        void set_image(QLabel*);

        QLabel* get_image() const;

private:
	QLabel* m_image;

public:
        layer(const std::string&);
        layer(const layer&);
        ~layer();

private:
        layer operator= (const layer&);

public:
	bool m_polygon_closed;
};

#endif // ROTO_DB_LAYER_H
