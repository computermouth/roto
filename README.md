## roto

[![pipeline status](https://gitlab.com/computermouth/roto/badges/master/pipeline.svg)](https://gitlab.com/computermouth/roto/commits/master)

## Builds

Builds will trigger with each commit [here](https://gitlab.com/computermouth/roto/pipelines).
Binary generated can be downloaded on the right side of that page as well.

Build process is simple `qmake -> make`

## Debian bullseye on

qt5-default is no longer a package, but qtbase5-dev works just fine
